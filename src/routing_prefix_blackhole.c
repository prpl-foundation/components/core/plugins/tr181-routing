/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#include <amxc/amxc.h>
#include <amxp/amxp.h>
#include <amxd/amxd_dm.h>
#include <amxd/amxd_transaction.h>

#include <debug/sahtrace.h>
#include <debug/sahtrace_macros.h>

#include <stdlib.h>
#include <string.h>

#include "netmodel/client.h"
#include "routing_prefix_blackhole.h"

/**********************************************************
* Type definitions
**********************************************************/

#define DEFAULT_PREFIX_BLACKHOLE_ALIAS "prefix_blackhole-default"
#define string_empty(x) ((x == NULL) || (*x == '\0'))

/**********************************************************
* Variable declarations
**********************************************************/
static netmodel_query_t* blackhole_query = NULL;

/**********************************************************
* Function Prototypes
**********************************************************/

/**********************************************************
* Functions
**********************************************************/

static void routing_prefix_blackhole_update(const char* prefix, uint32_t prefix_len) {
    SAH_TRACEZ_IN(ME);
    amxd_trans_t trans;
    amxd_object_t* prefix_blackhole_instance = NULL;
    amxd_status_t status = amxd_status_unknown_error;
    amxc_string_t prefix_blackhole_path;

    amxd_trans_init(&trans);
    amxc_string_init(&prefix_blackhole_path, 0);

    amxc_string_setf(&prefix_blackhole_path, "Routing.Router.1.IPv6Forwarding.%s.", DEFAULT_PREFIX_BLACKHOLE_ALIAS);

    prefix_blackhole_instance = amxd_dm_findf(routing_manager_get_dm(), "%s", amxc_string_get(&prefix_blackhole_path, 0));
    when_null_trace(prefix_blackhole_instance, exit, ERROR, "Could not find the default prefix blackhole instance '%s'", amxc_string_get(&prefix_blackhole_path, 0));
    amxd_trans_select_object(&trans, prefix_blackhole_instance);

    if(string_empty(prefix) || (prefix_len == 0)) {
        SAH_TRACEZ_INFO(ME, "Disabling blackhole prefix");
        amxd_trans_set_value(bool, &trans, "Enable", false);
        amxd_trans_set_value(cstring_t, &trans, "DestIPPrefix", "");
    } else {
        amxc_string_t blackhole_prefix;
        amxc_string_init(&blackhole_prefix, 0);
        amxc_string_setf(&blackhole_prefix, "%s/%u", prefix, prefix_len);

        SAH_TRACEZ_INFO(ME, "Blackhole prefix: %s/%u", prefix, prefix_len);
        amxd_trans_set_value(bool, &trans, "Enable", true);
        amxd_trans_set_value(cstring_t, &trans, "DestIPPrefix", amxc_string_get(&blackhole_prefix, 0));
        amxd_trans_set_value(cstring_t, &trans, "Type", "Blackhole");
        amxd_trans_set_value(cstring_t, &trans, "Interface", GET_CHAR(amxo_parser_get_config(routing_manager_get_parser(), "ip_intf_lo"), NULL));
        amxd_trans_set_value(cstring_t, &trans, "NextHop", "");

        amxc_string_clean(&blackhole_prefix);
    }

    status = amxd_trans_apply(&trans, routing_manager_get_dm());
    when_failed_trace(status, exit, ERROR, "Failed to execute transaction for %s", amxc_string_get(&prefix_blackhole_path, 0));

exit:
    amxd_trans_clean(&trans);
    amxc_string_clean(&prefix_blackhole_path);
    SAH_TRACEZ_OUT(ME);
}

static void blackhole_prefix_changed_cb(UNUSED const cstring_t const sig_name,
                                        const amxc_var_t* const data,
                                        UNUSED void* const priv) {
    SAH_TRACEZ_IN(ME);
    const char* prefix = GETP_CHAR(data, "0.Options.0.Value.Prefix");
    uint32_t prefix_len = GETP_UINT32(data, "0.Options.0.Value.PrefixLen");

    when_true_trace(prefix_len == 64, exit, INFO, "No blackhole required for this prefix '%s/%d'", prefix, prefix_len);
    routing_prefix_blackhole_update(prefix, prefix_len);

exit:
    SAH_TRACEZ_OUT(ME);
    return;
}

void open_blackhole_prefix_query(void) {
    SAH_TRACEZ_IN(ME);
    blackhole_query = netmodel_openQuery_getDHCPOption("Device.Logical.Interface.1.", "routing-manager", "req6", 25, "down", blackhole_prefix_changed_cb, NULL);
    SAH_TRACEZ_OUT(ME);
}

void close_blackhole_prefix_query(void) {
    SAH_TRACEZ_IN(ME);
    netmodel_closeQuery(blackhole_query);
    SAH_TRACEZ_OUT(ME);
}
