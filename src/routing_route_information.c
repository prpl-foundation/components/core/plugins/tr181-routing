/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2021 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include "routing_route_information.h"
#include "netmodel/client.h"
#include "routing_manager.h"
#include "raoption.h"

#include <amxd/amxd_dm.h>
#include <amxd/amxd_object.h>
#include <amxd/amxd_object_event.h>
#include <amxd/amxd_transaction.h>

#include <stdlib.h>
#include <string.h>
#include <sys/socket.h>
#include <netinet/icmp6.h>
#include <sys/ioctl.h>
#include <net/if.h>
#include <errno.h>
#include <arpa/inet.h>
#include <stdio.h>

/**********************************************************
* Type definitions
**********************************************************/

#define UINT8T_MAX_STRING_SIZE 4
#define HEADER_OPTION_PREFIX_INFORMATION 3

#define TIME_NOT_KNOWN "0001-01-01T00:00:00Z"
#define TIME_INFINITE "9999-12-31T23:59:59Z"

#define MAX_ROUTER_ADVERTISEMENT_MSG_LENGTH 1500

/**********************************************************
* Variable declarations
**********************************************************/

/**********************************************************
* Function Prototypes
**********************************************************/

static void socket_write_cb(int fd, void* priv);
static amxd_status_t handle_ra_sockaddr(struct sockaddr_in6* addr, amxd_trans_t* trans);
static void add_route_information(struct nd_router_advert* hdr, amxd_trans_t* trans);
static amxd_status_t handle_route_information_options(struct nd_opt_hdr* option, ssize_t len, amxd_object_t* obj, amxd_trans_t* trans, uint32_t* lifetime_seconds_ret);
static amxd_status_t get_existing_options(amxd_object_t* options_obj, amxc_var_t* existing_options);
static void add_option(struct nd_opt_hdr* option, amxd_object_t* options_obj, amxd_trans_t* trans_to_add, amxc_var_t* existing_options, uint32_t option_length);
static amxd_status_t remove_expired_options(amxd_object_t* options_obj, amxc_var_t* expired_options);
static void handle_option_3(unsigned cstring_t binval, amxd_trans_t* trans, uint32_t* lifetime_seconds_ret);
static amxd_status_t execute_intf_changes(amxd_object_t* obj, amxd_trans_t* trans, uint32_t lifetime_seconds);
static amxd_status_t set_timeout(intf_info_t* intf, uint32_t sec);
static void timeout_cb(amxp_timer_t* timer, void* priv);
static amxd_status_t clear_intf_parameters(amxd_object_t* obj);
static void clear_intf_parameters_options (amxd_trans_t* trans, amxd_object_t* obj);

/**********************************************************
* Functions
**********************************************************/

static void socket_write_cb(int fd, void* priv) {
    SAH_TRACEZ_IN(ME);
    static union {
        uint8_t raw[MAX_ROUTER_ADVERTISEMENT_MSG_LENGTH];
        uint32_t align;
        struct nd_router_advert hdr;
    } buf;
    amxd_status_t status = amxd_status_unknown_error;
    intf_info_t* intf = NULL;
    ssize_t len;
    socklen_t addrlen;
    struct sockaddr_in6 addr;
    amxd_trans_t trans;
    uint32_t lifetime_seconds = 0;

    amxd_trans_init(&trans);
    intf = (intf_info_t*) priv;

    when_null_trace(intf, exit, NOTICE, "Failed to handle ra message, bad intf parameter");
    amxd_trans_select_object(&trans, intf->obj);
    amxd_trans_set_attr(&trans, amxd_tattr_change_ro, true);

    addrlen = sizeof(addr);
    len = recvfrom(fd, &buf, sizeof(buf), MSG_DONTWAIT, (struct sockaddr*) &addr, &addrlen);
    if((len < (ssize_t) sizeof(struct nd_router_advert)) || (buf.hdr.nd_ra_type != ND_ROUTER_ADVERT)) {
        SAH_TRACEZ_ERROR(ME, "Failed to handle ra message. Received wrong type: %d -> ignore", buf.hdr.nd_ra_type);
        goto exit;
    }

    status = handle_ra_sockaddr(&addr, &trans);
    when_failed_trace(status, exit, NOTICE, "Failed to handle ra sockaddr");

    (void) add_route_information(&buf.hdr, &trans);

    status = handle_route_information_options((struct nd_opt_hdr*) ((&buf.hdr) + 1), len, intf->obj, &trans, &lifetime_seconds);
    when_failed_trace(status, exit, NOTICE, "Failed to handle route information options");

    status = execute_intf_changes(intf->obj, &trans, lifetime_seconds);
    when_failed_trace(status, exit, NOTICE, "Failed to handle ra message, failed to execute intf_changes");

    SAH_TRACEZ_INFO(ME, "Successfully handled ra message with type: %u, code:%u", buf.hdr.nd_ra_type, buf.hdr.nd_ra_code);
exit:
    amxd_trans_clean(&trans);
    SAH_TRACEZ_OUT(ME);
    return;
}

static amxd_status_t handle_ra_sockaddr(struct sockaddr_in6* addr, amxd_trans_t* trans) {
    SAH_TRACEZ_IN(ME);
    amxd_status_t status = amxd_status_unknown_error;
    char addrstr[INET6_ADDRSTRLEN];

    when_null_trace(inet_ntop(AF_INET6, &addr->sin6_addr, addrstr, INET6_ADDRSTRLEN), exit, ERROR, "Failed to get the source router");
    amxd_trans_set_value(cstring_t, trans, "SourceRouter", addrstr);

    status = amxd_status_ok;
exit:
    SAH_TRACEZ_OUT(ME);
    return status;
}

static void add_route_information(struct nd_router_advert* hdr, amxd_trans_t* trans) {
    SAH_TRACEZ_IN(ME);
    amxd_trans_set_value(bool, trans, "ManagedAddressConfiguration", hdr->nd_ra_flags_reserved & ND_RA_FLAG_MANAGED);
    amxd_trans_set_value(bool, trans, "OtherConfiguration", hdr->nd_ra_flags_reserved & ND_RA_FLAG_OTHER);
    amxd_trans_set_value(bool, trans, "HomeAgent", hdr->nd_ra_flags_reserved & ND_RA_FLAG_HOME_AGENT);
    amxd_trans_set_value(uint32_t, trans, "ReachableTime", ntohl(hdr->nd_ra_reachable));
    amxd_trans_set_value(uint32_t, trans, "RetransTimer", ntohl(hdr->nd_ra_retransmit));
    amxd_trans_set_value(uint32_t, trans, "RouterLifetime", ntohs(hdr->nd_ra_router_lifetime));
    SAH_TRACEZ_OUT(ME);
}

static amxd_status_t handle_route_information_options(struct nd_opt_hdr* option, ssize_t len, amxd_object_t* obj, amxd_trans_t* trans, uint32_t* lifetime_seconds_ret) {
    SAH_TRACEZ_IN(ME);
    amxd_status_t status = amxd_status_invalid_value;
    uint32_t option_length = 0;
    amxc_var_t existing_options;
    amxd_object_t* options_obj = NULL;

    amxc_var_init(&existing_options);
    amxc_var_set_type(&existing_options, AMXC_VAR_ID_HTABLE);

    when_null_trace(obj, exit, NOTICE, "Bad input argument, option object");

    options_obj = amxd_object_get(obj, "Option");
    when_null_trace(options_obj, exit, NOTICE, "Failed to get the options object");

    get_existing_options(options_obj, &existing_options);

    for(uint32_t i = 0; i < (len - sizeof(struct nd_router_advert)); i += option_length) {
        option = (struct nd_opt_hdr*) ((uint8_t*) option + option_length);
        option_length = option->nd_opt_len * 8;

        if((option_length == 0) || (option_length > len - i - sizeof(struct nd_router_advert))) {
            SAH_TRACEZ_ERROR(ME, "Incorrect length for option with type: %d, option length: %d, currently at byte %d", option->nd_opt_type, option_length, i);
            goto exit;
        }

        if(option->nd_opt_type == HEADER_OPTION_PREFIX_INFORMATION) {
            handle_option_3((unsigned cstring_t) option, trans, lifetime_seconds_ret);
        }

        add_option(option, options_obj, trans, &existing_options, option_length);
    }
    status = remove_expired_options(options_obj, &existing_options);
exit:
    amxc_var_clean(&existing_options);
    SAH_TRACEZ_OUT(ME);
    return status;
}

static amxd_status_t get_existing_options(amxd_object_t* options_obj, amxc_var_t* existing_options) {
    SAH_TRACEZ_IN(ME);
    amxd_status_t status = amxd_status_ok;
    uint8_t option_tag = 0;

    when_null_trace(existing_options, exit, ERROR, "Bad input argument");
    when_null_trace(options_obj, exit, NOTICE, "Bad input argument, option object");

    amxd_object_for_each(instance, it, options_obj) {
        cstring_t option_value = NULL;
        amxd_object_t* option = amxc_container_of(it, amxd_object_t, it);

        option_tag = amxd_object_get_value(uint8_t, option, "Tag", &status);
        when_failed_trace(status, exit, ERROR, "Failed to get the Tag param");
        option_value = amxd_object_get_value(cstring_t, option, "Value", &status);
        if(status != amxd_status_ok) {
            SAH_TRACEZ_ERROR(ME, "Failed to get the Value param");
        } else {
            char option_tag_str[UINT8T_MAX_STRING_SIZE];
            snprintf(option_tag_str, sizeof(option_tag_str), "%d", option_tag);
            amxc_var_add_key(cstring_t, existing_options, option_tag_str, option_value);
        }
        free(option_value);
    }
exit:
    SAH_TRACEZ_OUT(ME);
    return status;
}

static void add_option(struct nd_opt_hdr* option, amxd_object_t* options_obj, amxd_trans_t* trans_to_add, amxc_var_t* existing_options, uint32_t option_length) {
    SAH_TRACEZ_IN(ME);
    char* dataptr = NULL;
    amxc_string_t option_value;
    const cstring_t value_option = 0;
    amxd_object_t* options_instance = NULL;
    char tag_str[UINT8T_MAX_STRING_SIZE];
    amxc_var_t var_value;
    amxc_ts_t ts;

    amxc_var_init(&var_value);
    amxc_string_init(&option_value, option_length * 2 + 1);

    when_null_trace(options_obj, exit, NOTICE, "Invalid obj param given");
    when_null_trace(option, exit, NOTICE, "Invalid option param given");
    when_null_trace(existing_options, exit, NOTICE, "Invalid existing_options param given");
    when_null_trace(trans_to_add, exit, NOTICE, "Invalid trans param given");

    dataptr = (char*) option;
    amxc_string_bytes_2_hex_binary(&option_value, dataptr, option_length, NULL);

    snprintf(tag_str, sizeof(tag_str), "%d", option->nd_opt_type);
    value_option = GET_CHAR(existing_options, tag_str);
    if(value_option == NULL) {
        amxd_trans_select_object(trans_to_add, options_obj);
        amxd_trans_add_inst(trans_to_add, option->nd_opt_type, NULL);
        amxd_trans_set_value(cstring_t, trans_to_add, "Value", amxc_string_get(&option_value, 0));
        amxd_trans_set_value(uint8_t, trans_to_add, "Tag", option->nd_opt_type);
    } else {
        options_instance = amxd_object_get_instance(options_obj, NULL, option->nd_opt_type);
        when_null_trace(options_instance, exit, ERROR, "Could not find the instance");
        amxd_trans_select_object(trans_to_add, options_instance);
        if(strcmp(value_option, amxc_string_get(&option_value, 0)) != 0) {
            amxc_var_set(cstring_t, &var_value, amxc_string_get(&option_value, 0));
            amxd_trans_set_param(trans_to_add, "Value", &var_value);
        }
        amxc_var_t* rm_key = amxc_var_take_key(existing_options, tag_str);
        amxc_var_delete(&rm_key);
    }

    amxc_ts_now(&ts);
    amxd_trans_set_value(amxc_ts_t, trans_to_add, "LastUpdate", &ts);
    amxd_trans_select_object(trans_to_add, amxd_object_get_parent(options_obj));

exit:
    amxc_var_clean(&var_value);
    amxc_string_clean(&option_value);
    SAH_TRACEZ_OUT(ME);
    return;
}

static amxd_status_t remove_expired_options(amxd_object_t* options_obj, amxc_var_t* expired_options) {
    SAH_TRACEZ_IN(ME);
    amxd_trans_t trans_rm_options;
    amxd_status_t status = amxd_status_unknown_error;
    const amxc_htable_t* ht_params = NULL;

    amxd_trans_init(&trans_rm_options);
    when_null_trace(options_obj, exit, NOTICE, "Bad input param, options_obj");
    when_null_trace(expired_options, exit, ERROR, "Bad input param, expired_options");

    amxd_trans_select_object(&trans_rm_options, options_obj);
    ht_params = amxc_var_constcast(amxc_htable_t, expired_options);
    amxc_htable_for_each(hit, ht_params) {
        const cstring_t key = amxc_htable_it_get_key(hit);
        amxd_trans_del_inst(&trans_rm_options, 0, key);
    }

    status = amxd_trans_apply(&trans_rm_options, routing_manager_get_dm());
    when_failed_trace(status, exit, ERROR, "Failed to apply transaction, to clear options");
exit:
    amxd_trans_clean(&trans_rm_options);
    SAH_TRACEZ_OUT(ME);
    return status;
}

static void handle_option_3(unsigned cstring_t binval, amxd_trans_t* trans, uint32_t* lifetime_seconds_ret) {
    SAH_TRACEZ_IN(ME);
    amxc_var_t ret_values;
    uint32_t seconds_to_add = 0;
    uint8_t prefix_length = 0;
    const cstring_t prefix = NULL;
    amxc_ts_t ts;
    amxc_string_t prefix_string;

    amxc_var_init(&ret_values);
    amxc_string_init(&prefix_string, 0);

    when_null(trans, exit);
    when_str_empty(binval, exit);
    raoption_parse(&ret_values, HEADER_OPTION_PREFIX_INFORMATION, binval);

    prefix = GET_CHAR(&ret_values, "Prefix");
    when_str_empty_trace(prefix, exit, ERROR, "Failed to get the prefix information");
    seconds_to_add = GET_UINT32(&ret_values, "ValidLifetime");
    prefix_length = amxc_var_constcast(uint8_t, GET_ARG(&ret_values, "PrefixLength"));
    when_false(amxc_ts_now(&ts) != -1, exit);

    if(seconds_to_add == 0) {
        amxc_ts_parse(&ts, TIME_NOT_KNOWN, strlen(TIME_NOT_KNOWN));
    } else if(seconds_to_add == UINT32_MAX) {
        amxc_ts_parse(&ts, TIME_INFINITE, strlen(TIME_INFINITE));
    } else {
        ts.sec += seconds_to_add;
        *lifetime_seconds_ret = seconds_to_add;
    }
    amxd_trans_set_value(amxc_ts_t, trans, "RouteLifetime", &ts);

    amxc_string_appendf(&prefix_string, "%s/%d", prefix, prefix_length);
    amxd_trans_set_value(cstring_t, trans, "Prefix", amxc_string_get(&prefix_string, 0));
exit:
    amxc_string_clean(&prefix_string);
    amxc_var_clean(&ret_values);
    SAH_TRACEZ_OUT(ME);
    return;
}

static amxd_status_t execute_intf_changes(amxd_object_t* obj, amxd_trans_t* trans, uint32_t lifetime_seconds) {
    SAH_TRACEZ_IN(ME);
    amxd_status_t status = amxd_status_invalid_value;
    intf_info_t* intf = NULL;

    when_null(obj, exit);
    when_null(obj->priv, exit);

    intf = (intf_info_t*) obj->priv;

    status = amxd_trans_apply(trans, routing_manager_get_dm());
    when_failed_trace(status, exit, ERROR, "Failed to apply transaction");

    if(intf->ts_last_message == NULL) {
        intf->ts_last_message = malloc(sizeof(amxc_ts_t));
    }
    amxc_ts_now(intf->ts_last_message);

    if(lifetime_seconds != 0) {
        set_status_route_information(obj, ROUTE_INFORMATION_STATUS_FORWARD);
        status = set_timeout(intf, lifetime_seconds);
    }
exit:
    SAH_TRACEZ_OUT(ME);
    return status;
}

static amxd_status_t clear_intf_parameters(amxd_object_t* obj) {
    SAH_TRACEZ_IN(ME);
    amxd_status_t status = amxd_status_invalid_value;
    amxd_trans_t trans;
    amxc_ts_t ts;
    intf_info_t* intf = NULL;

    amxd_trans_init(&trans);
    when_null_trace(obj, exit, NOTICE, "Did not get a valid parameter");
    when_null_trace(obj->priv, exit, NOTICE, "Failed to get the priv data from the object");

    intf = (intf_info_t*) obj->priv;
    amxd_trans_select_object(&trans, obj);
    amxd_trans_set_attr(&trans, amxd_tattr_change_ro, true);

    amxc_ts_parse(&ts, TIME_NOT_KNOWN, strlen(TIME_NOT_KNOWN));

    amxd_trans_set_value(bool, &trans, "ManagedAddressConfiguration", false);
    amxd_trans_set_value(bool, &trans, "OtherConfiguration", false);
    amxd_trans_set_value(bool, &trans, "HomeAgent", false);
    amxd_trans_set_value(uint32_t, &trans, "ReachableTime", 0);
    amxd_trans_set_value(uint32_t, &trans, "RetransTimer", 0);
    amxd_trans_set_value(uint32_t, &trans, "RouterLifetime", 0);

    amxd_trans_set_value(amxc_ts_t, &trans, "RouteLifetime", &ts);
    amxd_trans_set_value(cstring_t, &trans, "SourceRouter", "");
    amxd_trans_set_value(cstring_t, &trans, "Prefix", "");

    clear_intf_parameters_options(&trans, obj);

    status = amxd_trans_apply(&trans, routing_manager_get_dm());
    when_failed_trace(status, exit, ERROR, "Failed to apply transaction, to clear parameters");

    free(intf->ts_last_message);
    intf->ts_last_message = NULL;
exit:
    amxd_trans_clean(&trans);
    SAH_TRACEZ_OUT(ME);
    return status;
}

static void clear_intf_parameters_options (amxd_trans_t* trans, amxd_object_t* obj) {
    SAH_TRACEZ_IN(ME);
    amxd_object_t* options_obj = NULL;

    options_obj = amxd_object_get(obj, "Option");
    when_null_trace(options_obj, exit, NOTICE, "failed to find the option object");

    amxd_trans_select_object(trans, options_obj);

    amxd_object_for_each(instance, it, options_obj) {
        amxd_object_t* del_option = amxc_container_of(it, amxd_object_t, it);
        amxd_trans_del_inst(trans, amxd_object_get_index(del_option), NULL);
    }
exit:
    SAH_TRACEZ_OUT(ME);
    return;
}

void set_status_route_information(amxd_object_t* obj, int status) {
    SAH_TRACEZ_IN(ME);
    amxd_trans_t trans;

    amxd_trans_init(&trans);
    amxd_trans_select_object(&trans, obj);
    amxd_trans_set_attr(&trans, amxd_tattr_change_ro, true);
    if(status == ROUTE_INFORMATION_STATUS_ERROR) {
        amxd_trans_set_value(cstring_t, &trans, "Status", "Error");
    } else if(status == ROUTE_INFORMATION_STATUS_NO_FORWARD) {
        amxd_trans_set_value(cstring_t, &trans, "Status", "NoForwardingEntry");
    } else if(status == ROUTE_INFORMATION_STATUS_LIFETIME_TIMEOUT) {
        amxd_trans_set_value(cstring_t, &trans, "Status", "LifetimeTimeout");
    } else if(status == ROUTE_INFORMATION_STATUS_FORWARD) {
        amxd_trans_set_value(cstring_t, &trans, "Status", "ForwardingEntryCreated");
    } else {
        amxd_trans_set_value(cstring_t, &trans, "Status", "Error");
        SAH_TRACEZ_ERROR(ME, "Invalid status '%d' value is given", status);
        goto exit;
    }

    if(amxd_trans_apply(&trans, routing_manager_get_dm()) != amxd_status_ok) {
        SAH_TRACEZ_ERROR(ME, "Failed to set the status of an intf");
    }
exit:
    SAH_TRACEZ_OUT(ME);
    amxd_trans_clean(&trans);
}

static amxd_status_t set_timeout(intf_info_t* intf, uint32_t sec) {
    SAH_TRACEZ_IN(ME);
    amxd_status_t status = amxd_status_unknown_error;
    int ret = -1;
    uint32_t msec = sec * 1000;

    when_null_trace(intf, exit, ERROR, "Invalid intf_info_t parameter was given");

    if(msec < sec) {
        msec = UINT32_MAX;
    }

    if(intf->timer == NULL) {
        ret = amxp_timer_new(&intf->timer, timeout_cb, (void*) intf);
        when_false_trace(ret == 0, exit, ERROR, "Failed to create a timer");
    }

    ret = amxp_timer_start(intf->timer, msec);
    when_false_trace(ret == 0, exit, ERROR, "Failed to start the timer");
    status = amxd_status_ok;
exit:
    SAH_TRACEZ_OUT(ME);
    return status;
}

static void timeout_cb(UNUSED amxp_timer_t* timer, void* priv) {
    SAH_TRACEZ_IN(ME);
    amxd_status_t status = amxd_status_unknown_error;

    intf_info_t* intf = (intf_info_t*) priv;
    when_null(intf, exit);

    status = clear_intf_parameters(intf->obj);
    when_failed_trace(status, exit, ERROR, "Failed to remove ra information from dm");

    set_status_route_information(intf->obj, ROUTE_INFORMATION_STATUS_LIFETIME_TIMEOUT);
exit:
    SAH_TRACEZ_OUT(ME);
    return;
}

amxd_status_t open_socket(intf_info_t* intf) {
    SAH_TRACEZ_IN(ME);
    int ret = -1;
    struct icmp6_filter filter;
    struct ifreq ifr;
    amxd_status_t status = amxd_status_unknown_error;

    when_null_trace(intf, exit, ERROR, "Invalid intf_info_t parameter was given");
    when_str_empty_trace(intf->intf_name, exit, ERROR, "Invalid intf name parameter was given");
    when_false_trace(intf->socket_fd == -1, exit, WARNING, "Trying to open a socket that already is open");

    // Create socket
    intf->socket_fd = socket(AF_INET6, SOCK_RAW | SOCK_NONBLOCK, IPPROTO_ICMPV6);
    when_false_trace((intf->socket_fd >= 0), exit, ERROR, "Failed to execute 'socket'");

    // Set socket options
    ICMP6_FILTER_SETBLOCKALL(&filter);
    ICMP6_FILTER_SETPASS(ND_ROUTER_ADVERT, &filter);
    ret = setsockopt(intf->socket_fd, IPPROTO_ICMPV6, ICMP6_FILTER, &filter, sizeof(filter));
    when_false_trace((ret >= 0), exit, ERROR, "Failed to execute 'setsockopt'");

    // Bind to specific interface
    memset(&ifr, 0, sizeof(ifr));
    strncpy(ifr.ifr_name, intf->intf_name, sizeof(ifr.ifr_name) - 1);
    ret = setsockopt(intf->socket_fd, SOL_SOCKET, SO_BINDTODEVICE, (void*) &ifr, sizeof(ifr));
    when_false_trace((ret >= 0), exit, ERROR, "Failed to execute 'setsockopt' -> %s", strerror(errno));

    ret = amxo_connection_add(routing_manager_get_parser(), intf->socket_fd, socket_write_cb, NULL, AMXO_CUSTOM, (void*) intf);
    when_false_trace((ret == 0), exit, ERROR, "Failed to execute 'amxo_connection_add'");

    SAH_TRACEZ_INFO(ME, "Successfully opened a socket for interface: %s", intf->intf_name);

    status = amxd_status_ok;
exit:
    if(status != amxd_status_ok) {
        set_status_route_information(intf->obj, ROUTE_INFORMATION_STATUS_ERROR);
        if(intf->socket_fd > 0) {
            close(intf->socket_fd);
            intf->socket_fd = -1;
        }
    }
    SAH_TRACEZ_OUT(ME);
    return status;
}

amxd_status_t close_socket(intf_info_t* intf) {
    SAH_TRACEZ_IN(ME);
    int ret = -1;
    amxd_status_t status = amxd_status_unknown_error;

    when_null_trace(intf, exit, ERROR, "Invalid intf_info_t parameter was given");
    if(intf->socket_fd == -1) {
        SAH_TRACEZ_WARNING(ME, "Trying to close a socket that was not opened yet");
        status = amxd_status_ok;
        goto exit;
    }
    if(intf->timer != NULL) {
        ret = amxp_timer_stop(intf->timer);
        when_false_trace((ret == 0), exit, ERROR, "Failed to stop the timer");
        amxp_timer_delete(&intf->timer);
    }

    status = clear_intf_parameters(intf->obj);
    when_failed_trace(status, exit, ERROR, "Failed to remove ra information from dm");

    ret = amxo_connection_remove(routing_manager_get_parser(), intf->socket_fd);
    when_false_status((ret == 0), exit, status = amxd_status_invalid_arg);

    ret = close(intf->socket_fd);
    intf->socket_fd = -1;
    when_false_status((ret != -1), exit, status = amxd_status_invalid_arg);

    set_status_route_information(intf->obj, ROUTE_INFORMATION_STATUS_NO_FORWARD);
    SAH_TRACEZ_INFO(ME, "Successfully closed the socket, for interface with path: %s", intf->intf_path);
exit:
    if(status != amxd_status_ok) {
        set_status_route_information(intf->obj, ROUTE_INFORMATION_STATUS_ERROR);
    }
    SAH_TRACEZ_OUT(ME);
    return status;
}

amxd_status_t _last_advertisement_onread(amxd_object_t* const object,
                                         UNUSED amxd_param_t* const param,
                                         amxd_action_t reason,
                                         UNUSED const amxc_var_t* const args,
                                         amxc_var_t* const retval,
                                         UNUSED void* priv) {
    SAH_TRACEZ_IN(ME);
    amxd_status_t status = amxd_status_unknown_error;
    int ret = -1;
    intf_info_t* intf = NULL;
    amxc_ts_t ts_now;
    uint32_t seconds_past = 0;

    when_null(retval, exit);
    when_null(object, exit);
    when_false(reason == action_param_read, exit);

    if(object->priv != NULL) {
        intf = (intf_info_t*) object->priv;
        if(intf->ts_last_message != NULL) {
            amxc_ts_now(&ts_now);
            seconds_past = ts_now.sec - intf->ts_last_message->sec;
        }
    }

    ret = amxc_var_set_uint32_t(retval, seconds_past);
    when_false_trace(ret == 0, exit, ERROR, "Failed to set the last_advertisement parameter");

    status = amxd_status_ok;
exit:
    SAH_TRACEZ_OUT(ME);
    return status;
}
