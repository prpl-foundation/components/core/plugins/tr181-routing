/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2022 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include "common.h"
#include "routing_manager.h"
#include "netdev_interface_name.h"
#include "netmodel/common_api.h"
#include "routing_ipvforwarding_info.h"

#include <amxb/amxb.h>
#include <amxd/amxd_object.h>

#include <stdlib.h>
#include <string.h>

/**********************************************************
* Type definitions
**********************************************************/

/**********************************************************
* Macro definitions
**********************************************************/

#define NETDEV_LINK_INDEX_OFFSET (2)

/**********************************************************
* Variable declarations
**********************************************************/

static amxc_var_t interfaces;

/**********************************************************
* Function Prototypes
**********************************************************/

static cstring_t netdev_get_interface(int link);
static void netmodel_path_created_cb(UNUSED const cstring_t const sig_name, const amxc_var_t* const data, void* const priv);
static cstring_t netdev_get_ip_path(const cstring_t interface);
static void netmodel_interface_path_changed_cb(UNUSED const char* const sig_name,
                                               const amxc_var_t* const data,
                                               void* const priv);

/**********************************************************
* Functions
**********************************************************/

static cstring_t netdev_get_interface(int link) {
    SAH_TRACEZ_IN(ME);
    amxc_var_t query;
    amxc_string_t path;
    cstring_t interface = NULL;
    amxc_var_init(&query);
    amxc_string_init(&path, 0);

    amxc_string_setf(&path, "NetDev.Link.%d.", link);
    if(AMXB_STATUS_OK == amxb_get(routing_manager_get_netdev_context(), amxc_string_get(&path, 0), 0, &query, 10)) {
        const cstring_t name = GETP_CHAR(&query, "0.0.Name");
        if(name != NULL) {
            interface = strdup(name);
        }
    }

    amxc_string_clean(&path);
    amxc_var_clean(&query);
    SAH_TRACEZ_OUT(ME);
    return interface;
}

static void netmodel_path_created(const cstring_t interface, amxd_object_t* object) {
    SAH_TRACEZ_IN(ME);
    amxb_bus_ctx_t* ctx = netmodel_get_amxb_bus();
    amxc_string_t path;
    int ret = -1;

    amxc_string_init(&path, 0);

    when_str_empty_trace(interface, exit, ERROR, "Bad input interface variable");
    when_null_trace(object, exit, ERROR, "Bad input object variable");
    when_null_trace(ctx, exit, ERROR, "Failed to get the ctx for NetModel.");

    ret = amxc_string_setf(&path, "NetModel.Intf.[ NetDevName == '%s' ].", interface);
    when_false_trace((ret == 0), exit, ERROR, "Could not create a Netmodel path with interface %s", interface);
    ret = amxb_subscribe(ctx,
                         amxc_string_get(&path, 0),
                         "notification in ['dm:instance-added', 'dm:object-changed'] && path matches 'NetModel.Intf.[0-9]+.$'",
                         netmodel_path_created_cb,
                         object);
    when_false_trace(ret == 0, exit, ERROR, "Failed to subscribe to %s", amxc_string_get(&path, 0));
exit:
    SAH_TRACEZ_OUT(ME);
    amxc_string_clean(&path);
}

static void netmodel_path_created_cb(UNUSED const cstring_t const sig_name, const amxc_var_t* const data, void* const priv) {
    SAH_TRACEZ_IN(ME);
    amxd_object_t* object = (amxd_object_t*) priv;
    const cstring_t obj_path = NULL;
    route_forwarding_info_t* route_forwarding_info = NULL;

    when_null_trace(object, exit, ERROR, "Bad input parameter object");
    when_null_trace(data, exit, ERROR, "Bad input parameter data");
    route_forwarding_info = (route_forwarding_info_t*) object->priv;
    when_null_trace(route_forwarding_info, exit, ERROR, "Object did not have a valid route_forwarding_info");
    when_false_trace(route_forwarding_info->nm_query == NULL, exit, ERROR, "netmodel query should be empty");
    obj_path = GET_CHAR(data, "path");
    when_str_empty_trace(obj_path, exit, ERROR, "Could not get the obj_path param from the event");

    route_forwarding_info->nm_query = netmodel_openQuery_getFirstParameter(obj_path, "routing-manager", "InterfacePath", "ip", netmodel_traverse_up, netmodel_interface_path_changed_cb, (void*) route_forwarding_info);
    when_null_trace(route_forwarding_info->nm_query, exit, ERROR, "Failed to create an netmodel openquery for %s", obj_path);
    netdev_unsubscribe_get_ip_path(object);
exit:
    SAH_TRACEZ_OUT(ME);
    return;
}

static void netmodel_interface_path_changed_cb(UNUSED const char* const sig_name,
                                               const amxc_var_t* const data,
                                               void* const priv) {
    SAH_TRACEZ_IN(ME);
    const cstring_t new_interface_name = NULL;
    route_forwarding_info_t* route_forwarding_info = NULL;
    cstring_t current_interface_name = NULL;

    when_null_trace(data, exit, ERROR, "Bad input parameter data");
    new_interface_name = amxc_var_constcast(cstring_t, data);
    when_str_empty(new_interface_name, exit);
    when_null_trace(priv, exit, ERROR, "Bad input parameter priv");
    route_forwarding_info = (route_forwarding_info_t*) priv;
    when_null_trace(route_forwarding_info, exit, ERROR, "Event did not have a valid route_forwarding_info");
    when_null_trace(route_forwarding_info->obj, exit, ERROR, "Event did not have a valid route_forwarding_info object");

    if(route_forwarding_info->nm_query != NULL) {
        netmodel_closeQuery(route_forwarding_info->nm_query);
        route_forwarding_info->nm_query = NULL;
    }
    if(route_forwarding_info->nm_query_ipv4_addr != NULL) {
        netmodel_closeQuery(route_forwarding_info->nm_query_ipv4_addr);
        route_forwarding_info->nm_query_ipv4_addr = NULL;
    }

    current_interface_name = amxd_object_get_value(cstring_t, route_forwarding_info->obj, "Interface", NULL);
    amxd_object_set_value(cstring_t, route_forwarding_info->obj, "Interface", new_interface_name);
    when_str_empty_trace(current_interface_name, exit, ERROR, "Could not get the current interface_name from the object");
    amxc_var_add_key(cstring_t, &interfaces, current_interface_name, new_interface_name);
exit:
    free(current_interface_name);
    SAH_TRACEZ_OUT(ME);
    return;
}

static cstring_t netdev_get_ip_path(const cstring_t interface) {
    SAH_TRACEZ_IN(ME);
    cstring_t ip_path = NULL;
    int ret = 0;
    amxc_string_t interface_path;
    amxc_var_t* netmodel_result;

    amxc_string_init(&interface_path, 0);
    when_null_trace(interface, exit, ERROR, "Bad input parameter");

    ret = amxc_string_setf(&interface_path, "NetModel.Intf.[NetDevName == '%s'].", interface);
    when_false_trace((ret == 0), exit, ERROR, "Could not create a Netmodel path with interface %s", interface);
    netmodel_result = netmodel_getFirstParameter(amxc_string_get(&interface_path, 0), "InterfacePath", "ip", netmodel_traverse_up);
    when_null(netmodel_result, exit);
    ip_path = amxc_var_dyncast(cstring_t, netmodel_result);
exit:
    amxc_string_clean(&interface_path);
    amxc_var_delete(&netmodel_result);
    SAH_TRACEZ_OUT(ME);
    return ip_path;
}

void netdev_unsubscribe_get_ip_path(amxd_object_t* object) {
    SAH_TRACEZ_IN(ME);
    amxb_bus_ctx_t* ctx = netmodel_get_amxb_bus();
    int ret = 0;

    when_null_trace(object, exit, ERROR, "Bad input object variable");

    ret = amxb_unsubscribe(ctx,
                           "NetModel.Intf.",
                           netmodel_path_created_cb,
                           object);
    when_false_trace((ret == 0), exit, ERROR, "Could not remove a Netmodel path");
exit:
    SAH_TRACEZ_OUT(ME);
    return;
}

rmi_return_code_t netdev_set_interface_name(const cstring_t path, amxd_object_t* object) {
    SAH_TRACEZ_IN(ME);
    cstring_t interface = NULL;
    cstring_t interface_name = NULL;
    rmi_return_code_t rc = rmi_error;
    int index = -1;
    const cstring_t found_interface = NULL;

    when_str_empty_trace(path, exit, ERROR, "Bad input path variable");
    when_null_trace(object, exit, ERROR, "Bad input object variable");

    index = netdev_link_from_path(path);
    when_false_trace(index != 0, exit, ERROR, "Failed to get the index for %s", path);
    interface = netdev_get_interface(index);
    when_null_trace(interface, exit, ERROR, "Failed to find the interface name for %s", path);
    /*
       found_interface = GET_CHAR(&interfaces, interface);
       if(NULL == found_interface) {
        netmodel_path_created(interface, object);
        interface_name = netdev_get_ip_path(interface);
        if((interface_name == NULL) || (*interface_name == 0)) {
            //Wait for the subscription callback to return the interface name, use the interface var for now
            found_interface = interface;
        } else {
            amxc_var_add_key(cstring_t, &interfaces, interface, interface_name);
            netdev_unsubscribe_get_ip_path(object);
            found_interface = interface_name;
        }
       }
       amxd_object_set_value(cstring_t, object, "Interface", found_interface);
     */
    (void) netmodel_path_created;
    (void) netdev_get_ip_path;
    (void) found_interface;
    amxd_object_set_value(cstring_t, object, "Interface", interface);
    rc = rmi_ok;
exit:
    free(interface);
    free(interface_name);
    SAH_TRACEZ_OUT(ME);
    return rc;
}

int netdev_link_from_path(const cstring_t path) {
    SAH_TRACEZ_IN(ME);
    int index = 0;
    amxc_string_t str;
    amxc_llist_t words;

    amxc_string_init(&str, 0);
    amxc_llist_init(&words);

    when_null(path, exit);
    amxc_string_set(&str, path);
    if(AMXC_STRING_SPLIT_OK == amxc_string_split_to_llist(&str, &words, '.')) {
        const cstring_t netdev_index_word = amxc_string_get_text_from_llist(&words, NETDEV_LINK_INDEX_OFFSET);
        if(NULL != netdev_index_word) {
            index = atoi(netdev_index_word);
        }
    }

exit:
    amxc_llist_clean(&words, amxc_string_list_it_free);
    amxc_string_clean(&str);
    SAH_TRACEZ_OUT(ME);
    return index;
}

void netdev_interface_name_init(void) {
    SAH_TRACEZ_IN(ME);
    amxc_var_init(&interfaces);
    amxc_var_set_type(&interfaces, AMXC_VAR_ID_HTABLE);
    SAH_TRACEZ_OUT(ME);
}

void netdev_interface_name_cleanup(void) {
    SAH_TRACEZ_IN(ME);
    amxc_var_clean(&interfaces);
    SAH_TRACEZ_OUT(ME);
}
