/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#ifndef _GNU_SOURCE
#define _GNU_SOURCE
#endif

#include "common.h"
#include "ipv6_routes.h"
#include "ipvx_routes.h"
#include "routing_manager.h"
#include "routing_ipvforwarding_info.h"

#include <debug/sahtrace.h>
#include <debug/sahtrace_macros.h>

#include <stdarg.h>
#include <stddef.h>
#include <stdint.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <amxd/amxd_dm.h>
#include <amxd/amxd_object_expression.h>

#define IPV6_ROUTE_TYPE_NORMAL "Normal"
#define IPV6_ROUTE_TYPE_RECEIVE "Receive"
#define IPV6_ROUTE_TYPE_BLACKHOLE "Blackhole"
#define IPV6_ROUTE_TYPE_UNREACHABLE "Unreachable"
#define IPV6_ROUTE_TYPE_PROHIBIT "Prohibit"
#define IPV6_ROUTE_TYPE_ANYCAST "Anycast"
#define IPV6_ROUTE_TYPE_MULTICAST "Multicast"

typedef enum {
    Forwarding_DestIPPrefix,
    Forwarding_ForwardingMetric,
    Forwarding_Origin,
    Forwarding_NextHop,
    Forwarding_Type,
    Forwarding_Nr_
} forwarding_param_t;

static const char* forwarding_param_str[Forwarding_Nr_] = {
    "DestIPPrefix",
    "ForwardingMetric",
    "Origin",
    "NextHop",
    "Type"
};

static const char* forwarding_param_to_str(int param);
static amxd_status_t protocol_to_origin(int param,
                                        const amxc_var_t* const netdev_params,
                                        amxc_var_t* forwarding_params,
                                        const ipvx_routes_config_t* config);
static amxd_object_t* find_ipv6_route(amxc_htable_t* lut, amxc_var_t* params);
static amxd_status_t dest_ip_prefix_param(int param,
                                          const amxc_var_t* const netdev_params,
                                          amxc_var_t* forwarding_params,
                                          const ipvx_routes_config_t* config);
static amxd_status_t dest_type_param(int param,
                                     const amxc_var_t* const netdev_params,
                                     amxc_var_t* forwarding_params,
                                     const ipvx_routes_config_t* config);

static netdev_transform_t mapping[Forwarding_Nr_] = {
    [Forwarding_DestIPPrefix] = {"Dst", dest_ip_prefix_param},
    [Forwarding_ForwardingMetric] = {"Priority", copy_parameter},
    [Forwarding_Origin] = {"Protocol", protocol_to_origin},
    [Forwarding_NextHop] = {"Gateway", copy_parameter},
    [Forwarding_Type] = {"Type", dest_type_param},
};

static const ipvx_routes_config_t ipv6_config = {
    .netdev_query = "NetDev.Link.*.IPv6Route.*.",
    .object_template = "Routing.Router.main.IPv6Forwarding.",
    .to_str = forwarding_param_to_str,
    .mapping = mapping,
    .tr181_compl_transform = NULL,
    .size = Forwarding_Nr_
};

int ipv6_routes_add_route(ipv6_routes_t* self,
                          const amxc_var_t* const data) {
    SAH_TRACEZ_IN(ME);
    int rv = ipvx_routes_add_route(&self->route_event_manager, data, &ipv6_config);
    SAH_TRACEZ_OUT(ME);
    return rv;
}

int ipv6_routes_remove_route(ipv6_routes_t* self,
                             const amxc_var_t* const data) {
    SAH_TRACEZ_IN(ME);
    int rv = route_event_manager_netdev_route_event_remove(&(self->route_event_manager), data);
    SAH_TRACEZ_OUT(ME);
    return rv;
}

rmi_return_code_t ipv6_routes_initial_route_population(ipv6_routes_t* self) {
    SAH_TRACEZ_IN(ME);
    rmi_return_code_t rv = rmi_error;
    when_null(self, exit);
    rv = ipvx_routes_initial_route_population(&self->route_event_manager, &ipv6_config);

exit:
    SAH_TRACEZ_OUT(ME);
    return rv;

}

rmi_return_code_t ipv6_routes_init(ipv6_routes_t* self) {
    SAH_TRACEZ_IN(ME);
    rmi_return_code_t rc = rmi_error;

    when_null(self, exit);
    rc = route_event_manager_init(&self->route_event_manager);
    when_failed_trace(rc, exit, ERROR, "Failed to init ipv4_routes object");
    self->route_event_manager.finder = find_ipv6_route;

exit:
    SAH_TRACEZ_OUT(ME);
    return rc;
}

void ipv6_routes_cleanup(ipv6_routes_t* self) {
    SAH_TRACEZ_IN(ME);
    if(NULL != self) {
        route_event_manager_cleanup(&self->route_event_manager);
    }
    SAH_TRACEZ_OUT(ME);
}

static amxd_status_t protocol_to_origin(int param,
                                        const amxc_var_t* const netdev_params,
                                        amxc_var_t* forwarding_params,
                                        UNUSED const ipvx_routes_config_t* config) {
    SAH_TRACEZ_IN(ME);
    amxd_status_t rc = amxd_status_unknown_error;
    amxc_var_t* item = NULL;
    const char* protocol = NULL;

    when_null(netdev_params, exit);
    when_null(forwarding_params, exit);

    item = GETP_ARG(netdev_params, mapping[param].netdev_param);
    when_null(item, exit);

    protocol = amxc_var_constcast(cstring_t, item);
    when_null(protocol, exit);

    /**
     * For the moment redirect all to Automatic.
     * From NetDev IPv6Route:
     * string Protocol; //redirect kernel boot automatic | user defined from NetDev.ConversionTable.Protocol
     **/
    if(NULL != amxc_var_add_key(cstring_t,
                                forwarding_params,
                                forwarding_param_to_str(param),
                                "Automatic")) {
        rc = amxd_status_ok;
    }

exit:
    SAH_TRACEZ_OUT(ME);
    return rc;

}

static const char* forwarding_param_to_str(int param) {
    SAH_TRACEZ_IN(ME);
    SAH_TRACEZ_OUT(ME);
    return ((Forwarding_DestIPPrefix <= param) && (Forwarding_Nr_ > param))
           ? forwarding_param_str[param] : NULL;
}

static amxd_object_t* find_ipv6_route(amxc_htable_t* lut, amxc_var_t* params) {
    SAH_TRACEZ_IN(ME);
    amxd_object_t* route = NULL;
    amxd_object_t* instances = amxd_dm_findf(routing_manager_get_dm(), "Routing.Router.main.IPv6Forwarding.");
    amxp_expr_t expression;
    amxc_string_t filter;

    amxc_string_init(&filter, 0);

    when_null(params, exit);
    when_null(instances, exit);
    amxc_string_setf(&filter,
                     "DestIPPrefix==\"%s\" && NextHop==\"%s\" && Type==\"%s\"",
                     GETP_CHAR(params, "DestIPPrefix"),
                     GETP_CHAR(params, "NextHop"),
                     GETP_CHAR(params, "Type"));
    amxp_expr_init(&expression, amxc_string_get(&filter, 0));
    route = amxd_object_find_instance(instances, &expression);
    while(route != NULL) {
        if(ipvx_routes_table_contains_object(lut, route) == false) {
            break;
        }
        route = amxd_object_find_next_instance(route, &expression);
    }
    amxp_expr_clean(&expression);
exit:
    amxc_string_clean(&filter);
    SAH_TRACEZ_OUT(ME);
    return route;
}

static amxd_status_t dest_ip_prefix_param(int param,
                                          const amxc_var_t* const netdev_params,
                                          amxc_var_t* forwarding_params,
                                          const ipvx_routes_config_t* config) {
    SAH_TRACEZ_IN(ME);
    amxd_status_t rc = amxd_status_unknown_error;
    amxc_var_t* dest_ip_prefix_to_add = NULL;
    amxc_string_t dst;
    amxc_var_t* dst_length_var = NULL;
    uint8_t dst_length = -1;

    amxc_string_init(&dst, 0);
    amxc_var_new(&dest_ip_prefix_to_add);

    when_null(netdev_params, exit);
    when_null(forwarding_params, exit);
    when_null(config, exit);
    when_null(config->to_str, exit);

    dst_length_var = GET_ARG(netdev_params, "DstLen");
    when_null(dst_length_var, exit);
    dst_length = amxc_var_dyncast(uint8_t, dst_length_var);

    amxc_string_setf(&dst, "%s/%d", GET_CHAR(netdev_params, config->mapping[param].netdev_param), dst_length);
    amxc_var_set_cstring_t(dest_ip_prefix_to_add, amxc_string_get(&dst, 0));
    if(0 == amxc_var_set_key(forwarding_params,
                             config->to_str(param),
                             dest_ip_prefix_to_add,
                             AMXC_VAR_FLAG_COPY)) {
        rc = amxd_status_ok;
    }

exit:
    amxc_var_delete(&dest_ip_prefix_to_add);
    amxc_string_clean(&dst);
    SAH_TRACEZ_OUT(ME);
    return rc;
}

static amxd_status_t dest_type_param(int param,
                                     const amxc_var_t* const netdev_params,
                                     amxc_var_t* forwarding_params,
                                     const ipvx_routes_config_t* config) {
    SAH_TRACEZ_IN(ME);
    amxd_status_t rc = amxd_status_unknown_error;
    amxc_var_t* dest_type_to_add = NULL;
    const cstring_t type_value = NULL;

    amxc_var_new(&dest_type_to_add);

    when_null(netdev_params, exit);
    when_null(forwarding_params, exit);
    when_null(config, exit);
    when_null(config->to_str, exit);

    type_value = GET_CHAR(netdev_params, config->mapping[param].netdev_param);
    if(strcmp(type_value, "unicast") == 0) {
        amxc_var_set_cstring_t(dest_type_to_add, IPV6_ROUTE_TYPE_NORMAL);
    } else if(strcmp(type_value, "local") == 0) {
        amxc_var_set_cstring_t(dest_type_to_add, IPV6_ROUTE_TYPE_RECEIVE);
    } else if(strcmp(type_value, "blackhole") == 0) {
        amxc_var_set_cstring_t(dest_type_to_add, IPV6_ROUTE_TYPE_BLACKHOLE);
    } else if(strcmp(type_value, "unreachable") == 0) {
        amxc_var_set_cstring_t(dest_type_to_add, IPV6_ROUTE_TYPE_UNREACHABLE);
    } else if(strcmp(type_value, "prohibit") == 0) {
        amxc_var_set_cstring_t(dest_type_to_add, IPV6_ROUTE_TYPE_PROHIBIT);
    } else if(strcmp(type_value, "anycast") == 0) {
        amxc_var_set_cstring_t(dest_type_to_add, IPV6_ROUTE_TYPE_ANYCAST);
    } else if(strcmp(type_value, "multicast") == 0) {
        amxc_var_set_cstring_t(dest_type_to_add, IPV6_ROUTE_TYPE_MULTICAST);
    } else {
        SAH_TRACEZ_WARNING(ME, "Unknown route type %s -> ignore route", type_value);
        goto exit;
    }

    if(0 == amxc_var_set_key(forwarding_params,
                             config->to_str(param),
                             dest_type_to_add,
                             AMXC_VAR_FLAG_COPY)) {
        rc = amxd_status_ok;
    }
exit:
    amxc_var_delete(&dest_type_to_add);
    SAH_TRACEZ_OUT(ME);
    return rc;
}
