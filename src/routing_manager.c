/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#include <stdlib.h>
#include <string.h>

#include <amxc/amxc.h>
#include <amxp/amxp.h>
#include <amxd/amxd_dm.h>
#include <amxd/amxd_types.h>
#include <amxd/amxd_action.h>
#include <amxd/amxd_object.h>
#include <debug/sahtrace.h>
#include <debug/sahtrace_macros.h>

#include "netdev_integration.h"
#include "netdev_interface_name.h"
#include "routing_dm_manager.h"
#include "routing_ipvforwarding_info.h"
#include "routing_ipvforwarding.h"
#include "routing_route_information_netmodel.h"
#include "routing_prefix_blackhole.h"

static routing_manager_t routing_manager;
static routing_manager_app_t app;

rmi_return_code_t routing_manager_init(void) {
    SAH_TRACEZ_IN(ME);
    rmi_return_code_t rc = rmi_error;

    subscription_manager_init(&routing_manager.subscription_manager);

    rc = ipv4_routes_init(&routing_manager.ipv4_routes);
    when_failed_trace(rc, exit, ERROR, "IPv4Routes initialization failed");

    rc = ipv6_routes_init(&routing_manager.ipv6_routes);
    when_failed_trace(rc, exit, ERROR, "IPv6Routes initialization failed");

    rc = netdev_subscribe_events(&routing_manager);
    when_failed_trace(rc, exit, ERROR, "Failed to subscribe for NetDev events");

    rc = ipv4_routes_initial_route_population(&routing_manager.ipv4_routes);
    when_failed_trace(rc, exit, ERROR, "Unable to populate initial IPv4 forwarding");

    rc = ipv6_routes_initial_route_population(&routing_manager.ipv6_routes);
    when_failed_trace(rc, exit, ERROR, "Unable to populate initial IPv6 forwarding");

exit:
    SAH_TRACEZ_OUT(ME);
    return rc;
}

void routing_manager_cleanup(void) {
    SAH_TRACEZ_IN(ME);
    subscription_manager_cleanup(&routing_manager.subscription_manager);
    ipv4_routes_cleanup(&routing_manager.ipv4_routes);
    ipv6_routes_cleanup(&routing_manager.ipv6_routes);
    SAH_TRACEZ_OUT(ME);
}

amxd_dm_t* PRIVATE routing_manager_get_dm(void) {
    SAH_TRACEZ_IN(ME);
    SAH_TRACEZ_OUT(ME);
    return app.dm;
}

amxo_parser_t* PRIVATE routing_manager_get_parser(void) {
    SAH_TRACEZ_IN(ME);
    SAH_TRACEZ_OUT(ME);
    return app.parser;
}

amxb_bus_ctx_t* PRIVATE routing_manager_get_netdev_context(void) {
    SAH_TRACEZ_IN(ME);
    SAH_TRACEZ_OUT(ME);
    return amxb_be_who_has("NetDev.");
}

int _routing_manager_main(int reason, amxd_dm_t* dm, amxo_parser_t* parser) {
    SAH_TRACEZ_IN(ME);
    int retval = -1;

    switch(reason) {
    case AMXO_START:
        app.dm = dm;
        app.parser = parser;

        when_false_trace(netmodel_initialize(), exit, ERROR, "Failed to init netmodel");

        netdev_interface_name_init();
        routing_dm_mngr_init();
        if(routing_manager_init() != 0) {
            routing_manager_cleanup();
        }
        routing_route_information_init();
        routing_ipvforwarding_init();
        open_blackhole_prefix_query();
        break;
    case AMXO_STOP:
        close_blackhole_prefix_query();
        routing_manager_cleanup();
        routing_route_information_cleanup();
        routing_ipvforwarding_cleanup();
        netdev_interface_name_cleanup();

        app.parser = NULL;
        app.dm = NULL;

        routing_dm_mngr_cleanup();
        app.so = NULL;
        netmodel_cleanup();
        break;
    }
    retval = 0;
exit:
    SAH_TRACEZ_OUT(ME);
    return retval;
}

amxd_status_t _check_is_empty_or_in(amxd_object_t* object,
                                    amxd_param_t* param,
                                    amxd_action_t reason,
                                    const amxc_var_t* const args,
                                    amxc_var_t* const retval,
                                    void* priv) {
    SAH_TRACEZ_IN(ME);
    amxd_status_t status = amxd_status_unknown_error;

    status = amxd_action_param_check_is_in(object, param, reason,
                                           args, retval, priv);

    if(status != amxd_status_ok) {
        char* input = amxc_var_dyncast(cstring_t, args);
        if((input == NULL) || (*input == 0)) {
            status = amxd_status_ok;
        }
        free(input);
    }

    SAH_TRACEZ_OUT(ME);
    return status;
}

void _print_event(UNUSED const char* const sig_name,
                  UNUSED const amxc_var_t* const data,
                  UNUSED void* const priv) {
#ifdef DM_TRACE
    printf("event received - %s\n", sig_name);
    if(data != NULL) {
        printf("Event data = \n");
        fflush(stdout);
        amxc_var_dump(data, STDOUT_FILENO);
    }
#endif
}

amxd_status_t routing_manager_call_ctrl(amxd_object_t* route, const char* action, amxc_var_t* data) {
    SAH_TRACEZ_IN(ME);
    amxd_status_t status = amxd_status_invalid_function;
    amxd_object_t* instance = amxd_dm_findf(routing_manager_get_dm(), "Routing.");
    amxc_var_t ret;
    const char* route_ctrl = NULL;
    int exec_success = -1;

    amxc_var_init(&ret);
    when_null_trace(instance, exit, ERROR, "Cannot find the Routing. object");
    when_null_trace(action, exit, ERROR, "Empty action parameter given");
    when_null_trace(route, exit, ERROR, "Empty route instance given");

    if(strcmp(action, ROUTING_ROUTER_ACTION_REMOVE) == 0) {
        route_event_manager_remove_object((IPV4 == (ipvx_t) GETP_UINT32(data, "IPVx") ? &routing_manager.ipv4_routes.route_event_manager : &routing_manager.ipv6_routes.route_event_manager), route);
    }
    route_ctrl = GET_CHAR(amxd_object_get_param_value(instance, ROUTING_CTRL), NULL);
    when_null_trace(route_ctrl, exit, ERROR, "Could not find the route_ctrl");
    exec_success = amxm_execute_function(route_ctrl, MOD_BRIDGE_CTRL, action, data, &ret);
    if((exec_success == 0) && (amxc_var_constcast(int32_t, &ret) != -1)) {
        status = amxd_status_ok;
    }
exit:
    amxc_var_clean(&ret);
    SAH_TRACEZ_OUT(ME);
    return status;
}

amxd_status_t _route_forwarding_deleted_ipvx_route(amxd_object_t* object,
                                                   amxd_param_t* param,
                                                   amxd_action_t reason,
                                                   const amxc_var_t* const args,
                                                   amxc_var_t* const retval,
                                                   void* priv) {
    SAH_TRACEZ_IN(ME);
    amxd_status_t status = amxd_action_object_destroy(object, param, reason, args, retval, priv);
    route_forwarding_info_t* info = NULL;

    when_failed(status, exit);
    when_null(object, exit);
    info = (route_forwarding_info_t*) object->priv;
    when_null(info, exit);
    if(info->removed_by_netdev) {
        netdev_unsubscribe_get_ip_path(object);
        goto exit;
    }

    route_event_manager_remove_object((&routing_manager.ipv4_routes.route_event_manager), object);
    (void) routing_ipvforwarding_disable_query(object);
exit:
    if(info != NULL) {
        route_forwarding_info_clear(object);
    }
    SAH_TRACEZ_OUT(ME);
    return status;
}
