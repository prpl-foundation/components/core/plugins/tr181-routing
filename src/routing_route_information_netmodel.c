/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2021 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include "routing_route_information_netmodel.h"
#include "routing_route_information.h"
#include "routing_manager.h"

#include <amxd/amxd_dm.h>
#include <amxd/amxd_object.h>
#include <amxd/amxd_object_event.h>
#include <amxd/amxd_action.h>
#include <amxd/amxd_transaction.h>

#include <stdlib.h>
#include <string.h>

/**********************************************************
* Type definitions
**********************************************************/

/**********************************************************
* Variable declarations
**********************************************************/

/**********************************************************
* Function Prototypes
**********************************************************/

static void subscribe_all_interfaces_netmodel(bool subscribe_enable);

static amxd_status_t create_intf_info(amxd_object_t* obj);

static void clear_intf_info(intf_info_t* info);

static void interface_setting_state_changed_cb(const char* const sig_name,
                                               const amxc_var_t* const data,
                                               void* const priv);

static bool get_route_information_enable_status(void);

/**********************************************************
* Functions
**********************************************************/

static void subscribe_all_interfaces_netmodel(bool subscribe_enable) {
    SAH_TRACEZ_IN(ME);
    amxd_object_t* interfaces = amxd_dm_findf(routing_manager_get_dm(), "Routing.RouteInformation.InterfaceSetting.");

    amxd_object_for_each(instance, it, interfaces) {
        amxd_object_t* interface = amxc_container_of(it, amxd_object_t, it);
        if(subscribe_enable) {
            if(interface->priv == NULL) {
                (void) create_intf_info(interface);
            }
        } else {
            clear_intf_info((intf_info_t*) interface->priv);
            interface->priv = NULL;
        }
    }
    SAH_TRACEZ_OUT(ME);
}

static bool get_route_information_enable_status(void) {
    SAH_TRACEZ_IN(ME);
    bool enable = false;
    amxd_object_t* location_enable_param = amxd_dm_findf(routing_manager_get_dm(), "Routing.RouteInformation.");

    when_null_trace(location_enable_param, exit, ERROR, "Failed to get the enable param from the dm");

    enable = amxd_object_get_value(bool, location_enable_param, "Enable", NULL);
exit:
    SAH_TRACEZ_OUT(ME);
    return enable;
}

static amxd_status_t create_intf_info(amxd_object_t* obj) {
    SAH_TRACEZ_IN(ME);
    intf_info_t* info = NULL;
    amxd_status_t status = amxd_status_object_not_found;
    cstring_t intf_path = NULL;

    when_false_status(get_route_information_enable_status(), exit, status = amxd_status_ok);
    when_null_trace(obj, exit, ERROR, "Failed to subscribe on netmodel, object was not found");
    when_false_trace(obj->priv == NULL, exit, ERROR, "Failed to subscribe on netmodel, priv was not null. For object: %s", obj->name);
    intf_path = amxd_object_get_value(cstring_t, obj, "Interface", NULL);
    if((intf_path == NULL) || (*intf_path == '\0')) {
        SAH_TRACEZ_ERROR(ME, "the intf path parameter is empty");
        set_status_route_information(obj, ROUTE_INFORMATION_STATUS_ERROR);
        free(intf_path);
        goto exit;
    }

    info = (intf_info_t*) calloc(1, sizeof(intf_info_t));
    obj->priv = info;
    info->obj = obj;
    info->socket_fd = -1;
    info->intf_path = intf_path;

    info->query = netmodel_openQuery_getFirstParameter(info->intf_path, "routing-manager", "NetDevName", "netdev-up && ipv6", netmodel_traverse_down, interface_setting_state_changed_cb, (void*) info);
    if(info->query != NULL) {
        SAH_TRACEZ_INFO(ME, "Successfully added a query to netmodel, for intf %s", info->intf_path);
        status = amxd_status_ok;
    } else {
        SAH_TRACEZ_ERROR(ME, "Failed to add a query to netmodel");
        set_status_route_information(obj, ROUTE_INFORMATION_STATUS_ERROR);
        clear_intf_info((intf_info_t*) obj->priv);
        obj->priv = NULL;
        status = amxd_status_unknown_error;
    }
exit:
    SAH_TRACEZ_OUT(ME);
    return status;
}

static void interface_setting_state_changed_cb(UNUSED const char* const sig_name,
                                               const amxc_var_t* const data,
                                               void* const priv) {
    SAH_TRACEZ_IN(ME);
    intf_info_t* info = (intf_info_t*) priv;
    amxd_status_t status = amxd_status_ok;
    bool state = false;
    const cstring_t intf_name = NULL;

    when_null(data, exit);
    when_null(info, exit);
    when_null(info->obj, exit);

    intf_name = amxc_var_constcast(cstring_t, data);
    if(info->intf_name != NULL) {
        status = close_socket(info);
        free(info->intf_name);
        info->intf_name = NULL;
    }

    if((intf_name != NULL) && (intf_name[0] != '\0')) {
        state = true;
        when_false_trace(info->socket_fd == -1, exit, WARNING, "Socket is already open, close the socket first");
        info->intf_name = strdup(intf_name);
        status = open_socket(info);
    }

    if(status == amxd_status_ok) {
        SAH_TRACEZ_INFO(ME, "Changed intf object state, with path %s and name %s to %d", info->intf_path, info->intf_name, state);
    } else {
        SAH_TRACEZ_ERROR(ME, "Failed to change intf object state, with path %s and name %s to %d", info->intf_path, info->intf_name, state);
    }
exit:
    SAH_TRACEZ_OUT(ME);
    return;
}

static void clear_intf_info(intf_info_t* info) {
    SAH_TRACEZ_IN(ME);
    when_null(info, exit);

    if(info->socket_fd != -1) {
        (void) close_socket(info);
    }

    if(info->query != NULL) {
        netmodel_closeQuery(info->query);
        info->query = NULL;
    }
    if(info->obj != NULL) {

        info->obj->priv = NULL;
        info->obj = NULL;
    }

    free(info->intf_name);
    info->intf_name = NULL;

    free(info->intf_path);
    info->intf_path = NULL;

    free(info);
exit:
    SAH_TRACEZ_OUT(ME);
    return;
}

void routing_route_information_init(void) {
    SAH_TRACEZ_IN(ME);
    subscribe_all_interfaces_netmodel(true);
    SAH_TRACEZ_OUT(ME);
}

void routing_route_information_cleanup(void) {
    SAH_TRACEZ_IN(ME);
    subscribe_all_interfaces_netmodel(false);
    SAH_TRACEZ_OUT(ME);
}

void _route_information_interface_added(UNUSED const char* const event_name,
                                        const amxc_var_t* const event_data,
                                        UNUSED void* const priv) {
    SAH_TRACEZ_IN(ME);
    amxd_object_t* intf_object = NULL;
    amxd_object_t* intf_instance = NULL;
    int instance_index = 0;

    when_null(event_data, exit);

    intf_object = amxd_dm_signal_get_object(routing_manager_get_dm(), event_data);
    when_null(intf_object, exit);
    instance_index = GET_UINT32(event_data, "index");

    intf_instance = amxd_object_get_instance(intf_object, NULL, instance_index);
    when_null(intf_instance, exit);

    if(intf_instance->priv == NULL) {
        (void) create_intf_info(intf_instance);
    }
exit:
    SAH_TRACEZ_OUT(ME);
    return;
}

amxd_status_t _route_information_deleted(amxd_object_t* object,
                                         amxd_param_t* param,
                                         amxd_action_t reason,
                                         const amxc_var_t* const args,
                                         amxc_var_t* const retval,
                                         void* priv) {
    SAH_TRACEZ_IN(ME);
    amxd_status_t status = amxd_status_unknown_error;

    if(object != NULL) {
        clear_intf_info((intf_info_t*) object->priv);
        object->priv = NULL;
    }

    status = amxd_action_object_destroy(object, param, reason, args, retval, priv);
    SAH_TRACEZ_OUT(ME);
    return status;
}

void _route_information_object_changed(UNUSED const char* const event_name,
                                       const amxc_var_t* const event_data,
                                       UNUSED void* const priv) {
    SAH_TRACEZ_IN(ME);

    amxd_object_t* interface = amxd_dm_signal_get_object(routing_manager_get_dm(), event_data);
    when_null(interface, exit);

    if(interface->priv != NULL) {
        clear_intf_info((intf_info_t*) interface->priv);
    }
    (void) create_intf_info(interface);

exit:
    SAH_TRACEZ_OUT(ME);
    return;
}

void _route_information_enable_changed(UNUSED const char* const event_name,
                                       UNUSED const amxc_var_t* const event_data,
                                       UNUSED void* const priv) {
    SAH_TRACEZ_IN(ME);
    subscribe_all_interfaces_netmodel(get_route_information_enable_status());
    SAH_TRACEZ_OUT(ME);
}
