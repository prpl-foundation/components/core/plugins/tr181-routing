/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2021 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <stdlib.h>
#include <string.h>
#include <debug/sahtrace.h>
#include <debug/sahtrace_macros.h>
#include <stdio.h>
#include <netinet/in.h>
#include <net/route.h>
#include <arpa/inet.h>
#include <sys/ioctl.h>
#include <net/if.h>
#include <linux/rtnetlink.h>

#include <amxc/amxc.h>
#include <amxc/amxc_macros.h>
#include <errno.h>

#include "ipvx_route_handler.h"

#include "mod_routing_lin.h"
#define ME "mod-routing"

struct _rtnlmsg {
    unsigned int buflen;
    unsigned short hdrlen;
    struct nlmsghdr* nlmsg;
    char buf[0];
};

typedef struct _rtnlmsg rtnlmsg_t;

static union {
    char raw[8192];
    rtnlmsg_t msg;
} static_rtnlmsg_buffer = {.msg = {.buflen = 8192, .hdrlen = 0, .nlmsg = (struct nlmsghdr*) static_rtnlmsg_buffer.msg.buf}};

static rtnlmsg_t* rtnlmsg_instance(void) {
    SAH_TRACEZ_IN(ME);
    SAH_TRACEZ_OUT(ME);
    return &static_rtnlmsg_buffer.msg;
}

static void rtnlmsg_addAttr(rtnlmsg_t* msg, unsigned short type, unsigned short len, const void* data) {
    SAH_TRACEZ_IN(ME);
    unsigned short rtalen = RTA_LENGTH(len);
    if(NLMSG_ALIGN(msg->nlmsg->nlmsg_len) + rtalen + ((char*) msg->nlmsg - (char*) msg) > msg->buflen) {
        SAH_TRACEZ_ERROR(ME, "Not enough room to add attribute %d to netlink message (%lu bytes short)", type,
                         (unsigned long) (NLMSG_ALIGN(msg->nlmsg->nlmsg_len) + rtalen + ((char*) msg->nlmsg - (char*) msg) - msg->buflen));
        goto exit;
    }
    struct rtattr* rta = (struct rtattr*) (((char*) msg->nlmsg) + NLMSG_ALIGN(msg->nlmsg->nlmsg_len));
    rta->rta_type = type;
    rta->rta_len = rtalen;
    if(len > 0) {
        memcpy(RTA_DATA(rta), data, len);
    }
    msg->nlmsg->nlmsg_len = NLMSG_ALIGN(msg->nlmsg->nlmsg_len) + rtalen;
exit:
    SAH_TRACEZ_OUT(ME);
}

static void rtnlmsg_initialize(rtnlmsg_t* msg, unsigned int buflen, unsigned short type, unsigned short hdrlen, unsigned short flags) {
    SAH_TRACEZ_IN(ME);
    memset(msg, 0, buflen);
    msg->buflen = buflen;
    msg->hdrlen = hdrlen;
    msg->nlmsg = (struct nlmsghdr*) msg->buf;
    msg->nlmsg->nlmsg_len = NLMSG_LENGTH(hdrlen);
    msg->nlmsg->nlmsg_type = type;
    msg->nlmsg->nlmsg_flags = flags;
    SAH_TRACEZ_OUT(ME);
}

static int rtnl_send(int fd, rtnlmsg_t* msg) {
    SAH_TRACEZ_IN(ME);
    int rv = -1;
    struct iovec iov;
    struct msghdr msghdr;
    struct sockaddr_nl addr;

    when_null_trace(msg, exit, ERROR, "No rtnetlink message provided");
    when_null_trace(msg->nlmsg, exit, ERROR, "Does not contain netlink message");

    memset(&msghdr, 0, sizeof(struct msghdr));
    memset(&addr, 0, sizeof(struct sockaddr_nl));

    msg->nlmsg->nlmsg_seq = ({static int nlmsg_seq = 0; ++nlmsg_seq;});
    msg->nlmsg->nlmsg_pid = 0;
    addr.nl_family = AF_NETLINK;

    iov.iov_base = msg->nlmsg;
    iov.iov_len = msg->nlmsg->nlmsg_len;

    msghdr.msg_name = &addr;
    msghdr.msg_namelen = sizeof(struct sockaddr_nl);
    msghdr.msg_iov = &iov;
    msghdr.msg_iovlen = 1;
    when_false_trace(sendmsg(fd, &msghdr, 0) > 0, exit, ERROR, "Failed to send netlink message, '%s'", strerror(errno));
    rv = 0;

exit:
    SAH_TRACEZ_OUT(ME);
    return rv;
}

static int nl_routing_action(const amxc_var_t* data, unsigned short action_type) {
    SAH_TRACEZ_IN(ME);
    int rv = -1;
    rtnlmsg_t* msg = rtnlmsg_instance();
    const char* interface_name = GETP_CHAR(data, "interface_name");
    int fd = socket(AF_NETLINK, SOCK_RAW, NETLINK_ROUTE);
    unsigned int index = 0;
    unsigned short flags = NLM_F_REQUEST;

    if(action_type == RTM_NEWROUTE) {
        flags |= NLM_F_CREATE | NLM_F_REPLACE;
    }
    rtnlmsg_initialize(msg, msg->buflen, action_type, sizeof(struct rtmsg), flags);

    struct rtmsg* rt = (struct rtmsg*) NLMSG_DATA(msg->nlmsg);
    rt->rtm_family = AF_INET;
    rt->rtm_table = RT_TABLE_MAIN;
    rt->rtm_protocol = RTPROT_BOOT;
    rt->rtm_scope = RT_SCOPE_UNIVERSE;
    rt->rtm_type = RTN_UNICAST;

    when_str_empty_trace(interface_name, exit, ERROR, "No interface name provided");
    index = if_nametoindex(interface_name);
    rtnlmsg_addAttr(msg, RTA_OIF, sizeof(unsigned int), &index);

    if((action_type != RTM_DELROUTE) && ((GET_INT32(data, "ForwardingMetric")) > 0)) {
        /* ForwardingMetric is an int32 which is either -1 (metric not to be used) or a positive integer.
           If it's larger than 0, it can safely be cast to a uint32 which is the data type in Netlink.*/
        uint32_t route_metric = GET_UINT32(data, "ForwardingMetric");
        rtnlmsg_addAttr(msg, RTA_PRIORITY, sizeof(route_metric), &route_metric);
    }

    rv = rtnl_send(fd, msg);

exit:
    close(fd);
    SAH_TRACEZ_OUT(ME);
    return rv;
}

int nl_route_add(const amxc_var_t* data) {
    SAH_TRACEZ_IN(ME);
    int rv = -1;
    ipvx_t version = IPV4;
    version = (ipvx_t) GETP_UINT32(data, "IPVx");
    if(IPV4 == version) {
        rv = nl_routing_action(data, RTM_NEWROUTE);
    } else {
        SAH_TRACEZ_ERROR(ME, "IPv6 is not yet supported with Origin IPCP");
    }
    SAH_TRACEZ_OUT(ME);
    return rv;
}

int nl_route_del(const amxc_var_t* data) {
    SAH_TRACEZ_IN(ME);
    int rv = -1;
    ipvx_t version = IPV4;
    version = (ipvx_t) GETP_UINT32(data, "IPVx");
    if(IPV4 == version) {
        rv = nl_routing_action(data, RTM_DELROUTE);
    } else {
        SAH_TRACEZ_ERROR(ME, "IPv6 is not yet supported with Origin IPCP");
    }
    SAH_TRACEZ_OUT(ME);
    return rv;
}
