/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2021 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <stdlib.h>
#include <string.h>
#include <debug/sahtrace.h>
#include <debug/sahtrace_macros.h>
#include <stdio.h>
#include <netinet/in.h>
#include <net/route.h>
#include <arpa/inet.h>
#include <sys/ioctl.h>
#include <net/if.h>
#include <linux/rtnetlink.h>

#include <amxc/amxc.h>
#include <amxc/amxc_macros.h>
#include <errno.h>

#include "ipvx_route_handler.h"

#define ME "mod-routing"

#define IPV6_ROUTE_TYPE_NORMAL "Normal"
#define IPV6_ROUTE_TYPE_RECEIVE "Receive"
#define IPV6_ROUTE_TYPE_BLACKHOLE "Blackhole"
#define IPV6_ROUTE_TYPE_UNREACHABLE "Unreachable"
#define IPV6_ROUTE_TYPE_PROHIBIT "Prohibit"
#define IPV6_ROUTE_TYPE_ANYCAST "Anycast"
#define IPV6_ROUTE_TYPE_MULTICAST "Multicast"

#define set_ipv4_address(route_item, address)  do {                        \
        struct sockaddr_in* addr = NULL;                                   \
        addr = (struct sockaddr_in*) &(route_item);                        \
        addr->sin_family = AF_INET;                                        \
        addr->sin_addr.s_addr = (address);                                 \
} while(0)

static int ipv4_route_action(const amxc_var_t* data, int fd, int action);
static int ipv6_route_action(const amxc_var_t* data, int fd, int action);
static int ipvx_route_action(const amxc_var_t* const data, int action);

int ipvx_route_add(const amxc_var_t* const data) {
    SAH_TRACEZ_IN(ME);
    int rv = ipvx_route_action(data, SIOCADDRT);
    SAH_TRACEZ_OUT(ME);
    return rv;
}

int ipvx_route_remove(const amxc_var_t* const data) {
    SAH_TRACEZ_IN(ME);
    int rv = ipvx_route_action(data, SIOCDELRT);
    SAH_TRACEZ_OUT(ME);
    return rv;
}

static int ipvx_route_action(const amxc_var_t* const data, int action) {
    SAH_TRACEZ_IN(ME);
    int ret = -1;
    ipvx_t version = IPV4;
    int fd = -1;

    when_null(data, exit);
    version = (ipvx_t) GETP_UINT32(data, "IPVx");
    fd = socket((IPV4 == version) ? AF_INET : AF_INET6, SOCK_DGRAM, IPPROTO_IP);
    when_true((-1 == fd), exit);

    if(IPV4 == version) {
        ret = ipv4_route_action(data, fd, action);
    } else {
        ret = ipv6_route_action(data, fd, action);
    }
exit:
    if(-1 != fd) {
        close(fd);
    }
    SAH_TRACEZ_OUT(ME);
    return ret;
}

static int ipv4_route_action(const amxc_var_t* data, int fd, int action) {
    SAH_TRACEZ_IN(ME);
    int ioctl_rc = -1;
    struct rtentry route;
    const cstring_t gateway = NULL;
    const cstring_t dest_ip_addr = NULL;
    const cstring_t subnet_mask = NULL;
    const cstring_t interface_name = NULL;
    int32_t route_metric = GET_INT32(data, "ForwardingMetric");

    memset(&route, 0, sizeof(route));

    gateway = GETP_CHAR(data, "GatewayIPAddress");
    gateway = (gateway == NULL || *gateway == '\0') ? "0.0.0.0" : gateway;
    set_ipv4_address(route.rt_gateway, inet_addr(gateway));
    dest_ip_addr = GETP_CHAR(data, "DestIPAddress");
    dest_ip_addr = (dest_ip_addr == NULL || *dest_ip_addr == '\0') ? "0.0.0.0" : dest_ip_addr;
    set_ipv4_address(route.rt_dst, inet_addr(dest_ip_addr));
    subnet_mask = GETP_CHAR(data, "DestSubnetMask");
    subnet_mask = (subnet_mask == NULL || *subnet_mask == '\0') ? "0.0.0.0" : subnet_mask;
    set_ipv4_address(route.rt_genmask, inet_addr(subnet_mask));

    interface_name = GETP_CHAR(data, "interface_name");
    if((interface_name != NULL) && (interface_name[0] != '\0')) {
        route.rt_dev = (char*) interface_name;
    }
    route.rt_flags = RTF_UP;
    if(strcmp(gateway, "0.0.0.0") != 0) {
        route.rt_flags |= RTF_GATEWAY;
    }
    if(route_metric > 0) {
        if(route_metric < INT32_MAX) {
            //route.rt_metric should be 1 higher than desired value. See route.h
            route_metric++;
        }
        route.rt_metric = route_metric;
    }
    ioctl_rc = ioctl(fd, action, &route);
    if(-1 == ioctl_rc) {
        SAH_TRACEZ_ERROR(ME, "Failed to perform an ipv4 action %d on route, Error: '%s' (%d)", action, strerror(errno), errno);
    }

    SAH_TRACEZ_OUT(ME);
    return ioctl_rc;
}

static int ipv6_route_action(const amxc_var_t* data, int fd, int action) {
    SAH_TRACEZ_IN(ME);
    int ioctl_rc = -1;
    struct in6_rtmsg route;
    uint8_t ipv6_length = 0;
    amxc_string_t ipv6_with_length;
    int8_t pos_backslash = -1;
    int ret = 0;
    const cstring_t interface_name = NULL;
    const cstring_t next_hop = NULL;
    const cstring_t type = NULL;
    int32_t route_metric = GET_INT32(data, "ForwardingMetric");

    amxc_string_init(&ipv6_with_length, 0);
    amxc_string_set(&ipv6_with_length, GETP_CHAR(data, "DestIPPrefix"));
    pos_backslash = amxc_string_search(&ipv6_with_length, "/", 0);
    when_false_trace(pos_backslash != -1, exit, ERROR, "DestIPPrefix has an invalid value");
    ipv6_length = atoi(amxc_string_get(&ipv6_with_length, pos_backslash + 1));
    ret = amxc_string_remove_at(&ipv6_with_length, pos_backslash, 5);
    when_false_trace(ret != -1, exit, ERROR, "Failed to remove prefix");

    next_hop = GETP_CHAR(data, "NextHop");
    when_null_trace(next_hop, exit, ERROR, "Failed to get the NextHop param");

    type = GETP_CHAR(data, "Type");
    when_null_trace(type, exit, ERROR, "Failed to get the type param");

    memset(&route, 0, sizeof(route));
    if(inet_pton(AF_INET6, amxc_string_get(&ipv6_with_length, 0), &route.rtmsg_dst) != 1) {
        SAH_TRACEZ_ERROR(ME, "Failed to parse the DestIPPrefix param");
        goto exit;
    }
    if((next_hop[0] != '\0') && (inet_pton(AF_INET6, next_hop, &route.rtmsg_gateway) != 1)) {
        SAH_TRACEZ_ERROR(ME, "Failed to parse the NextHop param");
        goto exit;
    }
    interface_name = GETP_CHAR(data, "interface_name");
    if(interface_name != NULL) {
        route.rtmsg_ifindex = (int) if_nametoindex(interface_name);
    }
    route.rtmsg_dst_len = ipv6_length;
    route.rtmsg_flags = RTF_UP;
    if(next_hop[0] != '\0') {
        route.rtmsg_flags |= RTF_GATEWAY;
    }

    if(strcmp(type, IPV6_ROUTE_TYPE_UNREACHABLE) == 0) {
        route.rtmsg_flags |= RTF_REJECT;
        route.rtmsg_type = RTN_UNREACHABLE;
    } else if(strcmp(type, IPV6_ROUTE_TYPE_BLACKHOLE) == 0) {
        route.rtmsg_type = RTN_BLACKHOLE;
    } else if(strcmp(type, IPV6_ROUTE_TYPE_PROHIBIT) == 0) {
        route.rtmsg_type = RTN_PROHIBIT;
    } else if(strcmp(type, IPV6_ROUTE_TYPE_RECEIVE) == 0) {
        route.rtmsg_type = RTN_LOCAL;
    } else if(strcmp(type, IPV6_ROUTE_TYPE_NORMAL) == 0) {
        route.rtmsg_type = RTN_UNICAST;
    } else if(strcmp(type, IPV6_ROUTE_TYPE_ANYCAST) == 0) {
        route.rtmsg_type = RTN_ANYCAST;
    } else if(strcmp(type, IPV6_ROUTE_TYPE_MULTICAST) == 0) {
        route.rtmsg_type = RTN_MULTICAST;
    } else {
        SAH_TRACEZ_ERROR(ME, "Failed to find a valid type %s", type);
    }

    if(route_metric > 0) {
        route.rtmsg_metric = route_metric;
    }
    ioctl_rc = ioctl(fd, action, &route);
    if(-1 == ioctl_rc) {
        SAH_TRACEZ_ERROR(ME, "Failed to perform an ipv6 action %d on route, Error: '%s' (%d)", action, strerror(errno), errno);
    }
exit:
    amxc_string_clean(&ipv6_with_length);
    SAH_TRACEZ_OUT(ME);
    return ioctl_rc;
}
