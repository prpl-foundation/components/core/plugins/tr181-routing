/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include "mock_socket.h"

#include <setjmp.h>
#include <cmocka.h>

#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#include <netinet/in.h>
#include <netinet/icmp6.h>

static char src_router_to_send[INET6_ADDRSTRLEN];
static uint8_t subnet_to_send;
static char seconds_to_send[5];
static char prefix_to_send[INET6_ADDRSTRLEN];
static int socket_counter = 999;

int __wrap_socket(UNUSED int domain, UNUSED int type, UNUSED int protocol) {
    socket_counter++;
    return socket_counter;
}

int __wrap_close(UNUSED int fd) {
    return 0;
}

int __wrap_setsockopt(UNUSED int socket, UNUSED int level, UNUSED int option_name, UNUSED const void* option_value, UNUSED socklen_t option_len) {
    return 0;
}

int __wrap_recvfrom(int sockfd, void* buf, size_t len, int flags, struct sockaddr* src_addr, UNUSED socklen_t* addrlen) {

    struct nd_router_advert* buf_send = NULL;
    struct sockaddr_in6* sockaddr = NULL;
    struct nd_opt_hdr* data_option = NULL;
    int length = sizeof(struct nd_router_advert);
    int option_length = 4;
    int location = 1;
    static char preferred_lifetime[5] = "aaaa";
    static char reserved[5] = "bbbb";

    assert_true(flags == MSG_DONTWAIT);
    assert_true(sockfd != -1);
    assert_true(sizeof(struct nd_router_advert) != len);

    // Set addr
    sockaddr = (struct sockaddr_in6*) src_addr;
    memcpy(&sockaddr->sin6_addr, src_router_to_send, strlen(src_router_to_send));

    // Set buffer
    buf_send = (struct nd_router_advert*) buf;
    buf_send->nd_ra_type = ND_ROUTER_ADVERT;

    // Set option
    data_option = (struct nd_opt_hdr*) (buf_send + 1);
    data_option->nd_opt_len = option_length;
    data_option->nd_opt_type = 3;

    memcpy((&data_option->nd_opt_len) + location, (char*) &subnet_to_send, sizeof(subnet_to_send));
    location += 1;
    location += sizeof(subnet_to_send);
    memcpy((&data_option->nd_opt_len) + location, seconds_to_send, strlen(seconds_to_send));
    location += strlen(seconds_to_send);
    memcpy((&data_option->nd_opt_len) + location, preferred_lifetime, strlen(preferred_lifetime));
    location += strlen(preferred_lifetime);
    memcpy((&data_option->nd_opt_len) + location, reserved, strlen(reserved));
    location += strlen(reserved);
    memcpy((&data_option->nd_opt_len) + location, prefix_to_send, strlen(prefix_to_send));

    return length + (option_length * 8);
}

void set_src_router_to_send(cstring_t src_router) {
    strcpy(src_router_to_send, src_router);
}

void set_data_to_send(uint8_t subnet, cstring_t seconds, cstring_t prefix) {
    subnet_to_send = subnet;
    strcpy(seconds_to_send, seconds);
    strcpy(prefix_to_send, prefix);
}
