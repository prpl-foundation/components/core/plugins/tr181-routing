/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include "mock_netmodel.h"

#include <setjmp.h>
#include <cmocka.h>

#include <stdlib.h>
#include <stdio.h>
#include <string.h>

static amxp_slot_fn_t has_flag_callback;
static void* has_flag_userdata;
static amxp_slot_fn_t openQuery2_callback;
static void* getFirstParameter2_userdata;
static amxp_slot_fn_t openQuery_isUp_callback;
static void* getFirstParameter_userdata;
static amxp_slot_fn_t openQuery_getAddrs_callback;
static void* getAddrs_userdata;
static const char* return_value_netmodel_getFirstParameter = NULL;

bool __wrap_netmodel_initialize(void) {
    return true;
}

void __wrap_netmodel_cleanup(void) {
    return;
}

void set_return_value_netmodel_getFirstParameter(const char* return_value) {
    return_value_netmodel_getFirstParameter = return_value;
}

amxc_var_t* __wrap_netmodel_getFirstParameter(const char* intf,
                                              const char* name,
                                              const char* flag,
                                              const char* traverse) {
    amxc_var_t* ret = NULL;

    assert_non_null(intf);
    assert_non_null(name);
    assert_non_null(flag);
    assert_non_null(traverse);

    amxc_var_new(&ret);
    amxc_var_set(cstring_t, ret, return_value_netmodel_getFirstParameter == NULL ? "" : return_value_netmodel_getFirstParameter);

    return ret;
}

netmodel_query_t* __wrap_netmodel_openQuery_hasFlag(const char* intf,
                                                    const char* subscriber,
                                                    const char* flag,
                                                    UNUSED const char* condition,
                                                    const char* traverse,
                                                    netmodel_callback_t handler,
                                                    void* userdata) {
    assert_non_null(traverse);
    assert_non_null(handler);
    assert_true(strcmp(subscriber, "routing-manager") == 0);
    assert_true(strncmp(intf, "Device.IP.Interface.", 20) == 0);
    assert_true(strcmp(flag, "ipv4-up") == 0);

    has_flag_userdata = userdata;
    has_flag_callback = handler;

    netmodel_query_t* q = malloc(sizeof(netmodel_query_t*));
    return q;
}

netmodel_query_t* __wrap_netmodel_openQuery_getFirstParameter(const char* intf,
                                                              const char* subscriber,
                                                              const char* name,
                                                              UNUSED const char* flag,
                                                              const char* traverse,
                                                              amxp_slot_fn_t handler,
                                                              void* userdata) {
    assert_non_null(traverse);
    assert_non_null(handler);
    assert_non_null(name);
    assert_true(strcmp(subscriber, "routing-manager") == 0);
    assert_true(strncmp(intf, "Device.IP.Interface.", 20) == 0);
    if(strcmp(name, "RemoteIPAddress") == 0) {
        openQuery2_callback = handler;
        getFirstParameter2_userdata = userdata;
    } else {
        openQuery_isUp_callback = handler;
        getFirstParameter_userdata = userdata;
    }

    netmodel_query_t* q = malloc(sizeof(netmodel_query_t*));
    return q;
}

netmodel_query_t* __wrap_netmodel_openQuery_getAddrs(const char* intf,
                                                     const char* subscriber,
                                                     UNUSED const char* flag,
                                                     const char* traverse,
                                                     amxp_slot_fn_t handler,
                                                     void* userdata) {
    assert_non_null(traverse);
    assert_non_null(handler);
    assert_true(strcmp(subscriber, "routing-manager") == 0);
    assert_true(strncmp(intf, "Device.IP.Interface.", 20) == 0);

    openQuery_getAddrs_callback = handler;
    getAddrs_userdata = userdata;

    netmodel_query_t* q = malloc(sizeof(netmodel_query_t*));
    return q;
}

void trigger_has_flag_callback(amxc_var_t* data) {
    assert_non_null(has_flag_callback);
    assert_non_null(has_flag_userdata);
    has_flag_callback(NULL, data, has_flag_userdata);
}

void trigger_getFirstParameter_callback(amxc_var_t* data) {
    assert_non_null(openQuery_isUp_callback);
    assert_non_null(getFirstParameter_userdata);
    openQuery_isUp_callback(NULL, data, getFirstParameter_userdata);
}

void trigger_getFirstParameter2_callback(amxc_var_t* data) {
    assert_non_null(openQuery2_callback);
    assert_non_null(getFirstParameter2_userdata);
    openQuery2_callback(NULL, data, getFirstParameter2_userdata);
}

void trigger_getAddrs_callback(amxc_var_t* data) {
    assert_non_null(openQuery_getAddrs_callback);
    assert_non_null(getAddrs_userdata);
    openQuery_getAddrs_callback(NULL, data, getAddrs_userdata);
}

void __wrap_netmodel_closeQuery(netmodel_query_t* query) {
    free(query);
}

amxp_slot_fn_t get_netmodel_openQuery_isUp_callback(void) {
    return openQuery_isUp_callback;
}