/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include "test_helper.h"

#include <stdlib.h>
#include <stdio.h>
#include <setjmp.h>
#include <stdarg.h>
#include <cmocka.h>

void setup_odl_with_mock_netdev_odl(const char* odl_file_path,
                                    amxd_dm_t* dm,
                                    amxo_parser_t* parser,
                                    amxb_bus_ctx_t** bus_ctx) {
    amxd_object_t* root_obj = NULL;

    assert_int_equal(amxd_dm_init(dm), amxd_status_ok);
    assert_int_equal(amxo_parser_init(parser), 0);
    assert_int_equal(test_register_dummy_be(), 0);

    root_obj = amxd_dm_get_root(dm);
    assert_non_null(root_obj);

    assert_int_equal(amxo_parser_parse_file(parser, "./mock_netdev.odl", root_obj), 0);
    assert_int_equal(amxo_parser_parse_file(parser, odl_file_path, root_obj), 0);

    // Create dummy/fake bus connections
    assert_int_equal(amxb_connect(bus_ctx, "dummy:/tmp/dummy.sock"), 0);
    assert_int_equal(amxo_connection_add(parser, amxb_get_fd(*bus_ctx), connection_read, "dummy:/tmp/dummy.sock", AMXO_BUS, *bus_ctx), 0);

    // Register data model
    assert_int_equal(amxb_register(*bus_ctx, dm), 0);
}

bool environment_is_setup(amx_test_environment_t* environment) {
    return environment->initialized;
}

int setup_environment_with_multiple_odl(const char** odl_file_paths,
                                        size_t number_of_file_paths,
                                        amx_test_environment_t* environment) {
    environment->initialized = false;

    amxd_dm_t* dm = &environment->dm;
    amxo_parser_t* parser = &environment->parser;
    amxb_bus_ctx_t* bus_ctx = NULL;

    amxd_object_t* root_obj = NULL;

    if(amxd_dm_init(dm) != amxd_status_ok) {
        goto error_exit;
    }

    if(amxo_parser_init(parser) != 0) {
        goto error_cleanup_1;
    }

    if(test_register_dummy_be() != 0) {
        goto error_cleanup_2;
    }

    root_obj = amxd_dm_get_root(dm);
    if(root_obj == NULL) {
        goto error_cleanup_3;
    }

    for(size_t idx = 0; idx < number_of_file_paths; ++idx) {
        if(amxo_parser_parse_file(parser, odl_file_paths[idx], root_obj) != 0) {
            goto error_cleanup_3;
        }
    }

    // Create dummy/fake bus connections
    if(amxb_connect(&bus_ctx, "dummy:/tmp/dummy.sock") != 0) {
        goto error_cleanup_3;
    }

    if(amxo_connection_add(parser, amxb_get_fd(bus_ctx), connection_read, "dummy:/tmp/dummy.sock", AMXO_BUS, bus_ctx) != 0) {
        goto error_cleanup_4;
    }

    // Register data model
    if(amxb_register(bus_ctx, dm) != 0) {
        goto error_cleanup_4;
    }

    environment->bus_ctx = bus_ctx;
    environment->initialized = true;

    return 0;

error_cleanup_4:
    amxb_free(&bus_ctx);
error_cleanup_3:
    test_unregister_dummy_be();
error_cleanup_2:
    amxo_parser_clean(parser);
error_cleanup_1:
    amxd_dm_clean(dm);
error_exit:
    return -1;

}

int clean_environment(amx_test_environment_t* environment) {
    environment->initialized = false;
    amxb_free(&environment->bus_ctx);
    test_unregister_dummy_be();
    amxo_parser_clean(&environment->parser);
    amxd_dm_clean(&environment->dm);

    return 0;
}
