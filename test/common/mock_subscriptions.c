/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include "common.h"
#include "mock_subscriptions.h"

#include <amxc/amxc.h>
#include <amxc/amxc_macros.h>

#include <amxp/amxp.h>
#include <amxp/amxp_signal.h>

#include <amxd/amxd_types.h>

#include <amxb/amxb.h>
#include <amxo/amxo.h>

#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

/*typedef struct _amxb_subscription {
    char* object;
    amxp_slot_fn_t slot_cb;
    void* priv;
    amxc_llist_it_t it;
   } amxb_subscription_t;*/

typedef struct _mock_subscription_entry {
    amxb_subscription_t* subscription;
    const char* expression;
    amxc_llist_it_t it;
} mock_subscription_entry_t;

static amxc_llist_t subscriptions;

static int subscription_new(amxb_subscription_t** subscription,
                            const char* object,
                            amxp_slot_fn_t slot_cb,
                            void* priv) {
    amxb_subscription_t* sub_obj = (amxb_subscription_t*) malloc(sizeof(amxb_subscription_t));
    when_null(sub_obj, error_exit);

    char* object_dup = strdup(object);
    when_null(object_dup, error_cleanup_1);

    sub_obj->object = object_dup;
    sub_obj->slot_cb = slot_cb;
    sub_obj->priv = priv;
    amxc_llist_it_init(&sub_obj->it);

    *subscription = sub_obj;

    return 0;

error_cleanup_1:
    free(sub_obj);
error_exit:
    return -1;
}

static void subscription_delete(amxb_subscription_t** subscription) {
    free((*subscription)->object);
    free(*subscription);

    *subscription = NULL;
}

static void mock_subscription_entry_delete(amxc_llist_it_t* it) {
    mock_subscription_entry_t* entry = amxc_container_of(it, mock_subscription_entry_t, it);

    subscription_delete(&entry->subscription);
    free((void*) entry->expression);
    free(entry);
}

int __wrap_amxb_subscription_new(UNUSED amxb_subscription_t** subscription,
                                 UNUSED amxb_bus_ctx_t* bus_ctx,
                                 UNUSED const char* object,
                                 UNUSED const char* expression,
                                 UNUSED amxp_slot_fn_t slot_cb,
                                 UNUSED void* priv) {
    int return_value = subscription_new(subscription,
                                        object,
                                        slot_cb,
                                        priv);
    when_failed(return_value, error_exit);

    const char* expression_dup = strdup(expression);
    when_null(expression_dup, error_cleanup_1);

    mock_subscription_entry_t* mock_entry = (mock_subscription_entry_t*) malloc(sizeof(mock_subscription_entry_t));
    when_null(mock_entry, error_cleanup_2);

    // Init on stack & memcpy to circumvent the malloced initialization of a const value
    mock_subscription_entry_t entry_proto = {
        .subscription = *subscription,
        .expression = expression_dup
    };
    when_failed(amxc_llist_it_init(&entry_proto.it), error_cleanup_2);
    memcpy(mock_entry, &entry_proto, sizeof(mock_subscription_entry_t));

    when_failed(amxc_llist_append(&subscriptions, &mock_entry->it), error_cleanup_2);

    //printf("Called subscription new\n");
    return 0;

error_cleanup_2:
    free((void*) expression_dup);
error_cleanup_1:
    subscription_delete(subscription);
error_exit:
    return -1;
}

int __wrap_amxb_subscription_delete(UNUSED amxb_subscription_t** subscription) {
    //assert(0);
    //printf("Called subscription delete\n");

    mock_subscription_entry_t* entry;

    amxc_llist_for_each(lit, &subscriptions) {
        entry = amxc_container_of(lit, mock_subscription_entry_t, it);

        if(entry->subscription == *subscription) {
            break;
        }

        entry = NULL;
    }

    if(entry == NULL) {
        return -1;
    }

    amxc_llist_it_take(&entry->it);
    mock_subscription_entry_delete(&entry->it);

    return 0;
}

void __wrap_amxb_subscription_remove_it(amxc_llist_it_t* it) {
    //assert(0);
    //printf("Called subscription remove it\n");
    amxb_subscription_t* subscription = amxc_container_of(it, amxb_subscription_t, it);
    __wrap_amxb_subscription_delete(&subscription);
}

int mock_subscriptions_init() {
    amxc_llist_init(&subscriptions);
    return 0;
}

int mock_subscriptions_clean() {
    // Signal if there are subscriptions left @ clean-up. In normal circumstances, every subscription should've been undone
    int return_value = amxc_llist_is_empty(&subscriptions) ? 0 : -1;

    // Clean subscriptions, even if correct behavior would've been that no subs are left. This is to have the mocking component not intervene too much with Valgrinds' results.
    amxc_llist_clean(&subscriptions, mock_subscription_entry_delete);
    return return_value;
}

amxc_llist_t* mock_subscriptions_get_subscription_entries() {
    return &subscriptions;
}

const char* mock_subscriptions_entry_it_get_object(amxc_llist_it_t* entry_it) {
    mock_subscription_entry_t* entry = amxc_container_of(entry_it, mock_subscription_entry_t, it);
    return entry->subscription->object;
}

amxp_slot_fn_t mock_subscriptions_entry_it_get_slot_cb(amxc_llist_it_t* entry_it) {
    mock_subscription_entry_t* entry = amxc_container_of(entry_it, mock_subscription_entry_t, it);
    return entry->subscription->slot_cb;
}

void* mock_subscription_entry_it_get_priv(amxc_llist_it_t* entry_it) {
    mock_subscription_entry_t* entry = amxc_container_of(entry_it, mock_subscription_entry_t, it);
    return entry->subscription->priv;
}
