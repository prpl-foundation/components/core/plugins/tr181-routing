/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2021 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include "test_route_ipvforwarding.h"
#include "routing_ipvforwarding_info.h"
#include "test_utils.h"

#include <stdlib.h>
#include <setjmp.h>
#include <cmocka.h>
#include <string.h>
#include <stdio.h>

#include "../mocks/mock_netmodel.h"

static void netdev_add_new_route(const cstring_t interface, const cstring_t Dst, const cstring_t gw, uint8_t dst_len, const cstring_t on_object) {
    amxb_bus_ctx_t* bus_ctx = NULL;
    amxc_var_t data_cb;
    amxc_var_t ret;
    amxc_string_t path;

    bus_ctx = amxb_be_who_has("NetDev.");
    amxc_string_init(&path, 0);

    amxc_var_init(&data_cb);
    amxc_var_init(&ret);
    amxc_var_set_type(&data_cb, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &data_cb, "Dst", Dst);
    amxc_var_add_key(cstring_t, &data_cb, "Gateway", gw);
    amxc_var_add_key(uint8_t, &data_cb, "DstLen", dst_len);

    amxc_string_setf(&path, "NetDev.Link.[Name == '%s'].%s.", interface, on_object);
    assert_true(amxb_add(bus_ctx, amxc_string_get(&path, 0), 0, NULL, &data_cb, &ret, 3) == AMXB_STATUS_OK);

    amxc_string_clean(&path);
    amxc_var_clean(&data_cb);
    amxc_var_clean(&ret);
}

static void netdev_del_route(const cstring_t interface, const cstring_t on_object, int index) {
    amxb_bus_ctx_t* bus_ctx = NULL;
    amxc_var_t ret;
    amxc_string_t path;

    bus_ctx = amxb_be_who_has("NetDev.");
    amxc_string_init(&path, 0);
    amxc_var_init(&ret);

    amxc_string_setf(&path, "NetDev.Link.[Name == '%s'].%s.", interface, on_object);
    assert_true(amxb_del(bus_ctx, amxc_string_get(&path, 0), index, NULL, &ret, 3) == AMXB_STATUS_OK);

    amxc_string_clean(&path);
    amxc_var_clean(&ret);
}

static void set_ip_route_enable(amxd_object_t* route_inst, bool enable) {
    amxd_trans_t trans;
    amxd_dm_t* dm = NULL;
    amxd_status_t status = amxd_status_unknown_error;

    dm = test_get_dm();

    amxd_trans_init(&trans);
    amxd_trans_select_object(&trans, route_inst);
    amxd_trans_set_value(bool, &trans, "Enable", enable);
    status = amxd_trans_apply(&trans, dm);
    assert_true(status == amxd_status_ok);

    amxd_trans_clean(&trans);
}

static void add_ipv4_route(cstring_t alias, cstring_t intf_name, cstring_t origin, cstring_t dest_ip, cstring_t subnet, cstring_t gw) {
    amxd_trans_t trans;
    amxd_dm_t* dm = NULL;
    amxd_object_t* route_ipvforwarding = NULL;
    amxd_status_t status = amxd_status_unknown_error;

    dm = test_get_dm();
    route_ipvforwarding = amxd_dm_findf(dm, "Routing.Router.1.IPv4Forwarding.");
    assert_non_null(route_ipvforwarding);

    amxd_trans_init(&trans);
    amxd_trans_select_object(&trans, route_ipvforwarding);
    amxd_trans_add_inst(&trans, 0, NULL);

    amxd_trans_set_value(cstring_t, &trans, "Interface", intf_name);
    amxd_trans_set_value(cstring_t, &trans, "Alias", alias);
    amxd_trans_set_value(cstring_t, &trans, "Origin", origin);
    amxd_trans_set_value(cstring_t, &trans, "DestIPAddress", dest_ip);
    amxd_trans_set_value(cstring_t, &trans, "DestSubnetMask", subnet);
    amxd_trans_set_value(cstring_t, &trans, "GatewayIPAddress", gw);
    amxd_trans_set_value(bool, &trans, "Enable", 1);
    status = amxd_trans_apply(&trans, dm);
    assert_true(status == amxd_status_ok);

    amxd_trans_clean(&trans);
}

static void add_ipv6_route(cstring_t alias, cstring_t intf_name, cstring_t origin, cstring_t dest_ip_prefix, cstring_t next_hop) {
    amxd_trans_t trans;
    amxd_dm_t* dm = NULL;
    amxd_object_t* route_ipvforwarding = NULL;
    amxd_status_t status = amxd_status_unknown_error;

    dm = test_get_dm();
    route_ipvforwarding = amxd_dm_findf(dm, "Routing.Router.1.IPv6Forwarding.");
    assert_non_null(route_ipvforwarding);

    amxd_trans_init(&trans);
    amxd_trans_select_object(&trans, route_ipvforwarding);
    amxd_trans_add_inst(&trans, 0, NULL);

    amxd_trans_set_value(cstring_t, &trans, "Interface", intf_name);
    amxd_trans_set_value(cstring_t, &trans, "Alias", alias);
    amxd_trans_set_value(cstring_t, &trans, "Origin", origin);
    amxd_trans_set_value(cstring_t, &trans, "DestIPPrefix", dest_ip_prefix);
    amxd_trans_set_value(cstring_t, &trans, "NextHop", next_hop);
    amxd_trans_set_value(bool, &trans, "Enable", 1);
    status = amxd_trans_apply(&trans, dm);
    assert_true(status == amxd_status_ok);

    amxd_trans_clean(&trans);
}

static void check_upgrade_persistent_params(amxd_object_t* instance, bool is_static) {
    assert_non_null(instance);
    amxd_object_for_each(parameter, it, instance) {
        amxd_param_t* param = amxc_container_of(it, amxd_param_t, it);
        if(amxd_param_is_attr_set(param, amxd_pattr_persistent)) {
            assert_int_equal(amxd_param_has_flag(param, "usersetting"), is_static);
        }
    }
}

void test_ppp_ipvforwarding_route(UNUSED void** state) {
    amxd_dm_t* dm = NULL;
    amxd_object_t* ip_route = NULL;
    amxc_var_t data_cb;
    amxc_var_t data_cb2;
    amxc_var_t data_has_flag_cb;
    cstring_t check_status = NULL;
    cstring_t check_gw_ip = NULL;
    bool check_static_route = 0;

    amxc_var_init(&data_cb);
    amxc_var_init(&data_cb2);
    amxc_var_init(&data_has_flag_cb);
    dm = test_get_dm();

    //TEST ENABLE
    add_ipv4_route("TestPpp1", "Device.IP.Interface.3.", "IPCP", "0.0.0.0", "0.0.0.0", "");
    test_handle_events();
    ip_route = amxd_dm_findf(dm, "Routing.Router.1.IPv4Forwarding.[Alias == 'TestPpp1']");
    assert_non_null(ip_route);

    amxc_var_set(cstring_t, &data_cb, "eth0");
    amxc_var_set(cstring_t, &data_cb2, "192.168.0.2");
    amxc_var_set(bool, &data_has_flag_cb, 1);
    trigger_has_flag_callback(&data_has_flag_cb);
    trigger_getFirstParameter_callback(&data_cb);
    trigger_getFirstParameter2_callback(&data_cb2);

    //CHECK enabled
    check_status = amxd_object_get_value(cstring_t, ip_route, "Status", NULL);
    check_gw_ip = amxd_object_get_value(cstring_t, ip_route, "GatewayIPAddress", NULL);
    check_static_route = amxd_object_get_value(bool, ip_route, "StaticRoute", NULL);
    assert_string_equal(check_gw_ip, "192.168.0.2");
    assert_string_equal(check_status, "Enabled");
    assert_false(check_static_route);
    check_upgrade_persistent_params(ip_route, false);
    free(check_status);
    free(check_gw_ip);

    //TEST Invalid intf
    add_ipv4_route("TestPpp2", "Device.IP.Interface.3.", "IPCP", "0.0.0.0", "0.0.0.0", "192.179.3.1");
    test_handle_events();
    ip_route = amxd_dm_findf(dm, "Routing.Router.1.IPv4Forwarding.[Alias == 'TestPpp2']");
    assert_non_null(ip_route);

    amxc_var_set(cstring_t, &data_cb, "");
    amxc_var_set(cstring_t, &data_cb2, "");
    amxc_var_set(bool, &data_has_flag_cb, 1);
    trigger_has_flag_callback(&data_has_flag_cb);
    trigger_getFirstParameter_callback(&data_cb);
    trigger_getFirstParameter2_callback(&data_cb2);
    test_handle_events();

    //CHECK
    check_status = amxd_object_get_value(cstring_t, ip_route, "Status", NULL);
    check_gw_ip = amxd_object_get_value(cstring_t, ip_route, "GatewayIPAddress", NULL);
    assert_string_equal(check_status, "Error");
    assert_string_equal(check_gw_ip, "");
    check_upgrade_persistent_params(ip_route, false);
    free(check_status);
    free(check_gw_ip);

    //TEST valid interface
    amxc_var_set(cstring_t, &data_cb, "eth0");
    amxc_var_set(cstring_t, &data_cb2, "192.168.0.3");
    trigger_getFirstParameter_callback(&data_cb);
    trigger_getFirstParameter2_callback(&data_cb2);

    //CHECK
    check_status = amxd_object_get_value(cstring_t, ip_route, "Status", NULL);
    check_gw_ip = amxd_object_get_value(cstring_t, ip_route, "GatewayIPAddress", NULL);
    assert_string_equal(check_status, "Enabled");
    assert_string_equal(check_gw_ip, "192.168.0.3");
    free(check_status);
    free(check_gw_ip);

    //TEST invalid gw
    amxc_var_set(cstring_t, &data_cb, "");
    amxc_var_set(cstring_t, &data_cb2, "");
    trigger_getFirstParameter_callback(&data_cb);
    trigger_getFirstParameter2_callback(&data_cb2);
    check_status = amxd_object_get_value(cstring_t, ip_route, "Status", NULL);
    check_gw_ip = amxd_object_get_value(cstring_t, ip_route, "GatewayIPAddress", NULL);
    assert_string_equal(check_status, "Error");
    assert_string_equal(check_gw_ip, "");
    free(check_status);
    free(check_gw_ip);

    amxc_var_clean(&data_cb);
    amxc_var_clean(&data_cb2);
    amxc_var_clean(&data_has_flag_cb);
}

void test_ppp_ipvforwarding_is_up_route(UNUSED void** state) {
    amxd_dm_t* dm = NULL;
    amxd_object_t* ip_route = NULL;
    amxc_var_t data_cb;
    amxc_var_t data_cb2;
    amxc_var_t data_has_flag_cb;
    cstring_t check_status = NULL;
    cstring_t check_gw_ip = NULL;
    bool check_static_route = 0;

    amxc_var_init(&data_cb);
    amxc_var_init(&data_cb2);
    amxc_var_init(&data_has_flag_cb);
    dm = test_get_dm();

    //TEST ENABLE
    add_ipv4_route("TestPppIsUp", "Device.IP.Interface.3.", "IPCP", "0.0.0.0", "0.0.0.0", "");
    test_handle_events();
    ip_route = amxd_dm_findf(dm, "Routing.Router.1.IPv4Forwarding.[Alias == 'TestPppIsUp']");
    assert_non_null(ip_route);

    amxc_var_set(cstring_t, &data_cb, "eth0");
    amxc_var_set(cstring_t, &data_cb2, "192.168.0.2");
    amxc_var_set(bool, &data_has_flag_cb, 1);
    trigger_has_flag_callback(&data_has_flag_cb);
    trigger_getFirstParameter_callback(&data_cb);
    trigger_getFirstParameter2_callback(&data_cb2);

    //CHECK enabled
    check_status = amxd_object_get_value(cstring_t, ip_route, "Status", NULL);
    check_gw_ip = amxd_object_get_value(cstring_t, ip_route, "GatewayIPAddress", NULL);
    check_static_route = amxd_object_get_value(bool, ip_route, "StaticRoute", NULL);
    assert_string_equal(check_gw_ip, "192.168.0.2");
    assert_string_equal(check_status, "Enabled");
    assert_false(check_static_route);
    check_upgrade_persistent_params(ip_route, false);
    free(check_status);
    free(check_gw_ip);

    //TEST DISABLE with is up
    amxc_var_set(bool, &data_has_flag_cb, 0);
    trigger_has_flag_callback(&data_has_flag_cb);

    //CHECK disabled
    check_status = amxd_object_get_value(cstring_t, ip_route, "Status", NULL);
    assert_string_equal(check_status, "Disabled");
    free(check_status);

    //TEST ENABLE with is up
    amxc_var_set(bool, &data_has_flag_cb, 1);
    trigger_has_flag_callback(&data_has_flag_cb);

    //CHECK enabled
    check_status = amxd_object_get_value(cstring_t, ip_route, "Status", NULL);
    assert_string_equal(check_status, "Enabled");
    free(check_status);

    amxc_var_clean(&data_cb);
    amxc_var_clean(&data_cb2);
    amxc_var_clean(&data_has_flag_cb);
}

void test_static_ipv4_ipvforwarding_route(UNUSED void** state) {
    amxd_dm_t* dm = NULL;
    amxd_object_t* ip_route = NULL;
    amxc_var_t data_cb;
    amxc_var_t* addrs_ht = NULL;
    cstring_t check_status = NULL;
    cstring_t check_gw_ip = NULL;
    bool check_static_route = 0;

    amxc_var_init(&data_cb);
    dm = test_get_dm();

    //TEST ENABLE
    add_ipv4_route("TestStatic1", "Device.IP.Interface.2.", "Static", "192.168.2.0", "255.255.255.0", "192.168.0.1");
    test_handle_events();
    ip_route = amxd_dm_findf(dm, "Routing.Router.1.IPv4Forwarding.[Alias == 'TestStatic1']");
    assert_non_null(ip_route);

    amxc_var_set_type(&data_cb, AMXC_VAR_ID_LIST);
    addrs_ht = amxc_var_add_new(&data_cb);
    amxc_var_set_type(addrs_ht, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, addrs_ht, "NetDevName", "eth0");
    trigger_getAddrs_callback(&data_cb);

    //CHECK enabled
    check_status = amxd_object_get_value(cstring_t, ip_route, "Status", NULL);
    check_gw_ip = amxd_object_get_value(cstring_t, ip_route, "GatewayIPAddress", NULL);
    check_static_route = amxd_object_get_value(bool, ip_route, "StaticRoute", NULL);
    assert_string_equal(check_status, "Enabled");
    assert_string_equal(check_gw_ip, "192.168.0.1");
    assert_true(check_static_route);
    check_upgrade_persistent_params(ip_route, true);
    free(check_status);
    free(check_gw_ip);

    amxc_var_clean(&data_cb);
}

void test_dhcp_ipvforwarding_route(UNUSED void** state) {
    amxd_dm_t* dm = NULL;
    amxd_object_t* ip_route = NULL;
    amxc_var_t data_cb;
    cstring_t check_status = NULL;
    cstring_t check_gw_ip = NULL;
    bool check_static_route = 0;

    amxc_var_init(&data_cb);
    dm = test_get_dm();

    //TEST ENABLE
    add_ipv4_route("TestDHCP", "Device.IP.Interface.2.", "DHCPv4", "0.0.0.0", "0.0.0.0", "");
    test_handle_events();
    ip_route = amxd_dm_findf(dm, "Routing.Router.1.IPv4Forwarding.[Alias == 'TestDHCP']");
    assert_non_null(ip_route);

    amxc_var_set(cstring_t, &data_cb, "192.168.0.2");
    trigger_getFirstParameter_callback(&data_cb);

    //CHECK enabled
    check_status = amxd_object_get_value(cstring_t, ip_route, "Status", NULL);
    check_gw_ip = amxd_object_get_value(cstring_t, ip_route, "GatewayIPAddress", NULL);
    check_static_route = amxd_object_get_value(bool, ip_route, "StaticRoute", NULL);
    assert_string_equal(check_status, "Enabled");
    assert_string_equal(check_gw_ip, "192.168.0.2");
    assert_false(check_static_route);
    check_upgrade_persistent_params(ip_route, false);
    free(check_status);
    free(check_gw_ip);

    //TEST DISABLE
    amxc_var_set(cstring_t, &data_cb, "");
    trigger_getFirstParameter_callback(&data_cb);

    //CHECK disabled
    check_status = amxd_object_get_value(cstring_t, ip_route, "Status", NULL);
    check_gw_ip = amxd_object_get_value(cstring_t, ip_route, "GatewayIPAddress", NULL);
    check_static_route = amxd_object_get_value(bool, ip_route, "StaticRoute", NULL);
    assert_string_equal(check_status, "Error");
    assert_string_equal(check_gw_ip, "");
    assert_false(check_static_route);
    check_upgrade_persistent_params(ip_route, false);
    free(check_status);
    free(check_gw_ip);

    amxc_var_clean(&data_cb);
    return;
}

void test_static_ipv6_ipvforwarding_route(UNUSED void** state) {
    amxd_dm_t* dm = NULL;
    amxd_object_t* ip_route = NULL;
    amxc_var_t data_cb;
    amxc_var_t* addrs_ht = NULL;
    cstring_t check_status = NULL;
    cstring_t check_next_hop = NULL;

    amxc_var_init(&data_cb);
    dm = test_get_dm();

    //TEST ENABLE
    add_ipv6_route("Testipv6", "Device.IP.Interface.2.", "Static", "fe88::/64", "");
    test_handle_events();
    ip_route = amxd_dm_findf(dm, "Routing.Router.1.IPv6Forwarding.[Alias == 'Testipv6']");
    assert_non_null(ip_route);

    amxc_var_set_type(&data_cb, AMXC_VAR_ID_LIST);
    addrs_ht = amxc_var_add_new(&data_cb);
    amxc_var_set_type(addrs_ht, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, addrs_ht, "NetDevName", "eth0");
    trigger_getAddrs_callback(&data_cb);

    //CHECK
    check_status = amxd_object_get_value(cstring_t, ip_route, "Status", NULL);
    check_next_hop = amxd_object_get_value(cstring_t, ip_route, "NextHop", NULL);
    assert_string_equal(check_status, "Enabled");
    assert_string_equal(check_next_hop, "");
    free(check_status);
    free(check_next_hop);

    amxc_var_clean(&data_cb);
}

void test_enable_trigger(UNUSED void** state) {
    amxd_dm_t* dm = NULL;
    amxd_object_t* ip_route = NULL;
    amxd_object_t* ip_route_parent = NULL;
    amxc_var_t data_cb;
    amxc_var_t* addrs_ht = NULL;
    cstring_t check_status = NULL;
    cstring_t check_gw_ip = NULL;
    bool check_static_route = 0;

    amxc_var_init(&data_cb);
    dm = test_get_dm();

    //TEST ENABLE
    add_ipv4_route("TestEnable", "Device.IP.Interface.2.", "Static", "192.168.2.0", "255.255.255.0", "192.168.0.1");
    test_handle_events();
    ip_route = amxd_dm_findf(dm, "Routing.Router.1.IPv4Forwarding.[Alias == 'TestEnable']");
    ip_route_parent = amxd_dm_findf(dm, "Routing.Router.1.");
    assert_non_null(ip_route);
    assert_non_null(ip_route_parent);

    amxc_var_set_type(&data_cb, AMXC_VAR_ID_LIST);
    addrs_ht = amxc_var_add_new(&data_cb);
    amxc_var_set_type(addrs_ht, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, addrs_ht, "NetDevName", "eth0");
    trigger_getAddrs_callback(&data_cb);

    //CHECK enabled
    check_status = amxd_object_get_value(cstring_t, ip_route, "Status", NULL);
    check_gw_ip = amxd_object_get_value(cstring_t, ip_route, "GatewayIPAddress", NULL);
    check_static_route = amxd_object_get_value(bool, ip_route, "StaticRoute", NULL);
    assert_string_equal(check_status, "Enabled");
    assert_string_equal(check_gw_ip, "192.168.0.1");
    assert_true(check_static_route);
    check_upgrade_persistent_params(ip_route, true);
    free(check_status);
    free(check_gw_ip);

    //TEST disable
    set_ip_route_enable(ip_route, 0);
    test_handle_events();

    //CHECK
    check_status = amxd_object_get_value(cstring_t, ip_route, "Status", NULL);
    assert_string_equal(check_status, "Disabled");
    free(check_status);

    //TEST enable
    set_ip_route_enable(ip_route, 1);
    test_handle_events();
    trigger_getAddrs_callback(&data_cb);

    check_status = amxd_object_get_value(cstring_t, ip_route, "Status", NULL);
    assert_string_equal(check_status, "Enabled");
    free(check_status);

    //TEST global disable
    set_ip_route_enable(ip_route_parent, 0);
    test_handle_events();

    //CHECK
    check_status = amxd_object_get_value(cstring_t, ip_route, "Status", NULL);
    assert_string_equal(check_status, "Disabled");
    free(check_status);

    set_ip_route_enable(ip_route_parent, 1);
    test_handle_events();

    amxc_var_clean(&data_cb);
}

void test_add_startup_ipv4_route_netdev(UNUSED void** state) {
    amxd_dm_t* dm = NULL;
    amxd_object_t* ip_route = NULL;
    amxc_var_t params;

    amxc_var_init(&params);
    dm = test_get_dm();

    //Check if netdev objects are made on start
    ip_route = amxd_dm_findf(dm, "NetDev.Link.[Name == 'eth0'].IPv4Route.[Dst == '192.168.10.0' && DstLen == 24 && Gateway == '']");
    assert_non_null(ip_route); // This object will be made by the netdev.odl file
    ip_route = amxd_dm_findf(dm, "NetDev.Link.[Name == 'eth0'].IPv4Route.[Dst == '192.168.11.0' && DstLen == 24 && Gateway == '192.168.10.1']");
    assert_non_null(ip_route); // This object will be made by the netdev.odl file

    //Check if the first object is copied
    ip_route = amxd_dm_findf(dm, "Routing.Router.1.IPv4Forwarding.[DestIPAddress == '192.168.10.0']");
    assert_non_null(ip_route);

    assert_int_equal(amxd_object_get_params(ip_route, &params, amxd_dm_access_protected), 0);
    assert_string_equal(GET_CHAR(&params, "Status"), "Enabled");
    assert_string_equal(GET_CHAR(&params, "GatewayIPAddress"), "");
    assert_string_equal(GET_CHAR(&params, "DestSubnetMask"), "255.255.255.0");
    assert_string_equal(GET_CHAR(&params, "Interface"), "eth0");
    assert_string_equal(GET_CHAR(&params, "Origin"), "Automatic");
    check_upgrade_persistent_params(ip_route, false);

    amxc_var_clean(&params);

    //Check if the second object is copied
    ip_route = amxd_dm_findf(dm, "Routing.Router.1.IPv4Forwarding.[DestIPAddress == '192.168.11.0']");
    assert_non_null(ip_route);

    assert_int_equal(amxd_object_get_params(ip_route, &params, amxd_dm_access_protected), 0);
    assert_string_equal(GET_CHAR(&params, "Status"), "Enabled");
    assert_string_equal(GET_CHAR(&params, "GatewayIPAddress"), "192.168.10.1");
    assert_string_equal(GET_CHAR(&params, "DestSubnetMask"), "255.255.255.0");
    assert_string_equal(GET_CHAR(&params, "Interface"), "eth0");
    assert_string_equal(GET_CHAR(&params, "Origin"), "Automatic");
    check_upgrade_persistent_params(ip_route, false);

    amxc_var_clean(&params);
}

void test_add_startup_ipv6_route_netdev(UNUSED void** state) {
    amxd_dm_t* dm = NULL;
    amxd_object_t* ip_route = NULL;
    amxc_var_t params;

    amxc_var_init(&params);
    dm = test_get_dm();

    //Check if netdev objects are made on start
    ip_route = amxd_dm_findf(dm, "NetDev.Link.[Name == 'eth0'].IPv6Route.[Dst == 'fe81::' && DstLen == 64 && Gateway == '']");
    assert_non_null(ip_route); // This object will be made by the netdev.odl file
    ip_route = amxd_dm_findf(dm, "NetDev.Link.[Name == 'eth0'].IPv6Route.[Dst == 'fe82::' && DstLen == 128 && Gateway == 'fe80::']");
    assert_non_null(ip_route); // This object will be made by the netdev.odl file

    //Check if the first object is copied
    ip_route = amxd_dm_findf(dm, "Routing.Router.1.IPv6Forwarding.[DestIPPrefix == 'fe81::/64']");
    assert_non_null(ip_route);

    assert_int_equal(amxd_object_get_params(ip_route, &params, amxd_dm_access_protected), 0);
    assert_string_equal(GET_CHAR(&params, "Status"), "Enabled");
    assert_string_equal(GET_CHAR(&params, "NextHop"), "");
    assert_string_equal(GET_CHAR(&params, "Interface"), "eth0");
    assert_string_equal(GET_CHAR(&params, "Origin"), "Automatic");
    check_upgrade_persistent_params(ip_route, false);

    amxc_var_clean(&params);

    //Check if the first object is copied
    ip_route = amxd_dm_findf(dm, "Routing.Router.1.IPv6Forwarding.[DestIPPrefix == 'fe82::/128']");
    assert_non_null(ip_route);

    assert_int_equal(amxd_object_get_params(ip_route, &params, amxd_dm_access_protected), 0);
    assert_string_equal(GET_CHAR(&params, "Status"), "Enabled");
    assert_string_equal(GET_CHAR(&params, "NextHop"), "fe80::");
    assert_string_equal(GET_CHAR(&params, "Interface"), "eth0");
    assert_string_equal(GET_CHAR(&params, "Origin"), "Automatic");

    amxc_var_clean(&params);
}

void test_add_remove_ipv4_route_netdev(UNUSED void** state) {
    amxd_object_t* ip_route = NULL;
    amxd_object_t* netdev_route = NULL;
    amxd_dm_t* dm = NULL;
    amxc_var_t params;
    const char* get_first_parameter_return_value = "Device.IP.Interface.1.";

    amxc_var_init(&params);
    dm = test_get_dm();

    //Create route using netdev
    set_return_value_netmodel_getFirstParameter(get_first_parameter_return_value);
    netdev_add_new_route("eth1", "192.168.20.0", "192.168.1.1", 16, "IPv4Route");
    test_handle_events();
    netdev_route = amxd_dm_findf(dm, "NetDev.Link.[Name == 'eth1'].IPv4Route.[Dst == '192.168.20.0' && DstLen == 16 && Gateway == '192.168.1.1']");
    assert_non_null(netdev_route);

    //Check if it is copied to routing
    ip_route = amxd_dm_findf(dm, "Routing.Router.1.IPv4Forwarding.[DestIPAddress == '192.168.20.0']");
    assert_non_null(ip_route);

    assert_int_equal(amxd_object_get_params(ip_route, &params, amxd_dm_access_protected), 0);
    assert_string_equal(GET_CHAR(&params, "Status"), "Enabled");
    assert_string_equal(GET_CHAR(&params, "GatewayIPAddress"), "192.168.1.1");
    assert_string_equal(GET_CHAR(&params, "DestSubnetMask"), "255.255.0.0");
    assert_string_equal(GET_CHAR(&params, "Origin"), "Automatic");
    check_upgrade_persistent_params(ip_route, false);

    //Replace later when converting names is back enabled
    //assert_string_equal(GET_CHAR(&params, "Interface"), "Device.IP.Interface.1.");
    assert_string_equal(GET_CHAR(&params, "Interface"), "eth1");

    //CHECK able to delete
    netdev_del_route("eth1", "IPv4Route", netdev_route->index);
    test_handle_events();

    netdev_route = amxd_dm_findf(dm, "NetDev.Link.[Name == 'eth1'].IPv4Route.[Dst == '192.168.20.0' && DstLen == 16 && Gateway == '192.168.1.1']");
    assert_null(netdev_route);
    ip_route = amxd_dm_findf(dm, "Routing.Router.1.IPv4Forwarding.[DestIPAddress == '192.168.20.0']");
    assert_null(ip_route);

    amxc_var_clean(&params);
}

void test_add_remove_ipv6_route_netdev(UNUSED void** state) {
    amxd_dm_t* dm = NULL;
    amxd_object_t* ip_route = NULL;
    amxd_object_t* netdev_route = NULL;
    amxc_var_t params;
    const char* get_first_parameter_return_value = "Device.IP.Interface.1.";

    amxc_var_init(&params);
    dm = test_get_dm();

    //Create route using netdev
    set_return_value_netmodel_getFirstParameter(get_first_parameter_return_value);
    netdev_add_new_route("eth1", "fe85::", "fe85::1", 64, "IPv6Route");
    test_handle_events();
    netdev_route = amxd_dm_findf(dm, "NetDev.Link.[Name == 'eth1'].IPv6Route.[Dst == 'fe85::' && DstLen == 64 && Gateway == 'fe85::1']");
    assert_non_null(netdev_route);

    //Check if the first object is copied
    ip_route = amxd_dm_findf(dm, "Routing.Router.1.IPv6Forwarding.[DestIPPrefix == 'fe85::/64']");
    assert_non_null(ip_route);

    assert_int_equal(amxd_object_get_params(ip_route, &params, amxd_dm_access_protected), 0);
    assert_string_equal(GET_CHAR(&params, "Status"), "Enabled");
    assert_string_equal(GET_CHAR(&params, "NextHop"), "fe85::1");
    assert_string_equal(GET_CHAR(&params, "Origin"), "Automatic");

    //Replace later when converting names is back enabled
    //assert_string_equal(GET_CHAR(&params, "Interface"), "Device.IP.Interface.1.");
    assert_string_equal(GET_CHAR(&params, "Interface"), "eth1");

    //CHECK able to delete
    netdev_del_route("eth1", "IPv6Route", netdev_route->index);
    test_handle_events();

    netdev_route = amxd_dm_findf(dm, "NetDev.Link.[Name == 'eth1'].IPv6Route.[Dst == 'fe85::' && DstLen == 64 && Gateway == 'fe85::1']");
    assert_null(netdev_route);
    ip_route = amxd_dm_findf(dm, "Routing.Router.1.IPv6Forwarding.[DestIPPrefix == 'fe85::/64']");
    assert_null(ip_route);

    amxc_var_clean(&params);
}
