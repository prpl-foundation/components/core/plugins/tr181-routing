/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#ifndef __IPVX_ROUTES_PRIV_H__
#define __IPVX_ROUTES_PRIV_H__

#ifdef __cplusplus
extern "C" {
#endif

#include "dm_interface.h"
#include "route_event_manager.h"

#include <amxc/amxc.h>

#include <amxp/amxp.h>
#include <amxp/amxp_signal.h>

#include <amxd/amxd_types.h>

#include <amxb/amxb.h>
#include <amxo/amxo.h>

struct ipvx_routes_config;
typedef const char*(* param_to_str)(int);
typedef amxd_status_t (* transform_fn_t)(int, const amxc_var_t* const, amxc_var_t*, const struct ipvx_routes_config*);
typedef amxd_status_t (* tr181_transform_fn_t)(amxc_var_t*);

typedef struct {
    const char* netdev_param;
    transform_fn_t transform;
} netdev_transform_t;

typedef struct ipvx_routes_config {
    const char* netdev_query;
    const char* object_template;
    const param_to_str to_str;
    const netdev_transform_t* const mapping;
    const tr181_transform_fn_t tr181_compl_transform;
    const uint32_t size;
} ipvx_routes_config_t;

rmi_return_code_t ipvx_routes_initial_route_population(route_event_manager_t* route_event_manager,
                                                       const ipvx_routes_config_t* config);

rmi_return_code_t ipvx_routes_add_route(route_event_manager_t* route_event_manager,
                                        const amxc_var_t* const event_data,
                                        const ipvx_routes_config_t* config);
amxd_status_t copy_parameter(int param,
                             const amxc_var_t* const netdev_params,
                             amxc_var_t* forwarding_params,
                             const ipvx_routes_config_t* config);
bool PRIVATE ipvx_routes_table_contains_object(amxc_htable_t* lut, amxd_object_t* obj);

#ifdef __cplusplus
}
#endif

#endif // __IPV4_ROUTES_PRIV_H__
