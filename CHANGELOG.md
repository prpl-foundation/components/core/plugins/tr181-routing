# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]


## Release v1.20.7 - 2024-10-28(16:36:17 +0000)

## Release v1.20.6 - 2024-10-16(15:45:16 +0000)

## Release v1.20.5 - 2024-09-30(18:00:45 +0000)

### Other

- - memory leak while receiving RA
- - memory leak while receiving RA

## Release v1.20.4 - 2024-09-10(17:58:01 +0000)

### Other

- [Security][AppArmor] Apparmor must be available on PRPL builds
- IPv6 RA DNS server still in use after disabling IPv6

## Release v1.20.3 - 2024-08-07(10:41:09 +0000)

### Other

- - [tr181-routing] global_ip_interfaces.odl is included in the wrong odl file

## Release v1.20.2 - 2024-08-02(15:01:55 +0000)

### Other

- add support for multiple runlevels

## Release v1.20.1 - 2024-07-23(07:40:43 +0000)

### Fixes

- Better shutdown script

## Release v1.20.0 - 2024-06-11(15:37:18 +0000)

### New

- - [Cellular] [routing] It must be possible to retrieve the default route from the Cellular DM

## Release v1.19.2 - 2024-04-10(10:01:36 +0000)

### Changes

- Make amxb timeouts configurable

## Release v1.19.1 - 2024-03-29(14:59:06 +0000)

### Fixes

- [tr181-pcm] Saved and defaults odl should not be included in the backup files

## Release v1.19.0 - 2024-03-25(08:40:48 +0000)

### New

- Add tr181-device proxy odl files to components

## Release v1.18.5 - 2024-03-18(06:45:42 +0000)

### Other

- Rework TR-181 interface stacks indexing number

## Release v1.18.4 - 2024-03-16(18:15:20 +0000)

### Other

- Rework TR-181 interface stacks indexing number

## Release v1.18.3 - 2024-03-13(12:21:46 +0000)

### Fixes

- [routing-manager] iprouter-up is set before the ra flags

## Release v1.18.2 - 2024-01-09(10:08:30 +0000)

### Fixes

- The default route is not set after switching ip modes in wan-manager

## Release v1.18.1 - 2023-12-15(13:11:05 +0000)

### Fixes

- Remove ToDos

## Release v1.18.0 - 2023-12-14(16:42:13 +0000)

### New

- [Routing] Make all IPv4Forwarding instances persistent by default.

## Release v1.17.0 - 2023-12-12(11:44:45 +0000)

### New

- [Routing] It must be possible to define routes with a configured metric.

## Release v1.16.2 - 2023-11-13(15:41:36 +0000)

### Fixes

- [Nokia-ap] There is no default route for nokia ap

## Release v1.16.1 - 2023-10-13(13:46:09 +0000)

### Changes

-  All applications using sahtrace logs should use default log levels

## Release v1.16.0 - 2023-10-09(07:50:04 +0000)

### New

- [amxrt][no-root-user][capability drop] tr181-routing plugin must be adapted to run as non-root and lmited capabilities

## Release v1.15.2 - 2023-09-19(15:11:01 +0000)

### Fixes

- [tr181-pcm] All the plugins that are register to PCM should set the pcm_svc_config

## Release v1.15.1 - 2023-06-28(14:37:33 +0000)

### Fixes

- No change event for IP forwarding status parameter

## Release v1.15.0 - 2023-06-16(08:25:40 +0000)

### New

- [tr181-pcm] Set the usersetting parameters for each plugin

## Release v1.14.0 - 2023-06-07(08:22:14 +0000)

### New

- add ACLs permissions for cwmp user

## Release v1.13.0 - 2023-05-30(10:23:31 +0000)

### New

- [CDROUTER][IPv6] The Box is still advertising itself as router at reception on WAN of RA with router lifetime 0

## Release v1.12.1 - 2023-04-27(09:33:52 +0000)

### Changes

- Switch custom vendor RouteInformation parameters to the new standard parameters

## Release v1.12.0 - 2023-04-21(17:33:50 +0000)

### New

- [DHCPv4Client][option 121] Dynamically create routes based on option121

## Release v1.11.0 - 2023-04-03(08:59:51 +0000)

### New

- Create an event when a renew is received

## Release v1.10.12 - 2023-03-23(12:51:22 +0000)

### Fixes

- Unhandled IPv6Route types

## Release v1.10.11 - 2023-03-23(09:53:44 +0000)

### Fixes

- Get for initial routes no longer works

## Release v1.10.10 - 2023-03-20(14:44:07 +0000)

### Other

- Use DHCP for setting default route on AP

## Release v1.10.9 - 2023-03-17(18:38:14 +0000)

### Other

- [baf] Correct typo in config option

## Release v1.10.8 - 2023-03-16(13:40:38 +0000)

### Other

- Add AP config files

## Release v1.10.7 - 2023-03-15(10:44:55 +0000)

### Fixes

- Add in/out traces to all the functions

## Release v1.10.6 - 2023-03-09(09:16:36 +0000)

### Other

- [Config] enable configurable coredump generation

## Release v1.10.5 - 2023-03-07(10:03:37 +0000)

### Fixes

- [wan-manager] DSLite static default route is incorrectly modified when switching WANModes

### Other

- Add missing runtime dependency on rpcd

## Release v1.10.4 - 2023-02-18(07:34:05 +0000)

### Fixes

- [routing] When switching WANModes, Routing.RouteInformation sometimes goes into an unrecoverable error state

## Release v1.10.3 - 2023-02-18(07:26:36 +0000)

### Fixes

- blackhole should in some cases not be made

## Release v1.10.2 - 2023-02-13(15:39:03 +0000)

### Fixes

- Fails to set default route when switching from ppp mode

## Release v1.10.1 - 2023-02-08(11:05:04 +0000)

### Fixes

- Fails to set default route when switching from ppp mode

## Release v1.10.0 - 2023-02-02(14:58:24 +0000)

### New

- [amx][routing]Create a null route for IPv6 prefix delegation

## Release v1.9.1 - 2023-01-14(08:34:41 +0000)

### Fixes

- [tr181][routing] their is no ppp default route

## Release v1.9.0 - 2023-01-09(13:46:26 +0000)

### New

- [import-dbg] Disable import-dbg by default for all amxrt plugin

## Release v1.8.5 - 2023-01-09(11:29:56 +0000)

### Fixes

- avoidable allocation with amxc_string_dup

## Release v1.8.4 - 2022-12-09(09:30:02 +0000)

### Fixes

- [Config] coredump generation should be configurable

## Release v1.8.3 - 2022-11-25(07:42:00 +0000)

### Other

- [DSLite][Routing] When DSLite is enabled, a default route must be created as well

## Release v1.8.2 - 2022-11-24(14:43:52 +0000)

### Fixes

- Handle static routes differently in routing manager

## Release v1.8.1 - 2022-10-26(08:15:54 +0000)

### Other

- [TR181][Routing] a 134 firewall rule is made by the routing-manager

## Release v1.8.0 - 2022-10-14(12:09:21 +0000)

### New

- Update the README

## Release v1.7.7 - 2022-10-14(09:01:48 +0000)

### Fixes

- [TR181][Routing] a 134 firewall rule is made by the routing-manager

## Release v1.7.6 - 2022-10-14(07:57:00 +0000)

### Fixes

- [TR181][Router] enable parameter automatically goes to 0

## Release v1.7.5 - 2022-09-14(14:14:44 +0000)

### Fixes

- Fixes for PPP default route

## Release v1.7.4 - 2022-06-28(11:49:44 +0000)

### Fixes

- [TR181][Routing] Disable resolving the netdev name

## Release v1.7.3 - 2022-06-24(08:39:14 +0000)

### Fixes

- [TR181][Router] subscribe on a more specific path

## Release v1.7.2 - 2022-06-16(07:02:23 +0000)

### Other

- [amxrt] All amx plugins should start with the -D option

## Release v1.7.1 - 2022-05-31(06:04:02 +0000)

### Fixes

- [TR181][Router] It must be possible to configure the default route based on the PPP(IPCP) Information

## Release v1.7.0 - 2022-05-30(13:49:42 +0000)

### New

- [TR181][Router] It must be possible to configure the default route based on the PPP(IPCP) Information

## Release v1.6.0 - 2022-05-16(06:05:26 +0000)

### New

- [Routing] It must be possible to represent an "unreachable" route in TR181

## Release v1.5.1 - 2022-05-11(09:32:24 +0000)

### Fixes

- [tr181-routing manager] Static route should be reboot persistent

## Release v1.5.0 - 2022-05-09(05:58:41 +0000)

### New

- [tr181-router]Dynamically detected routes should expose the TR181-interface path instead of the linux interface as Interface

## Release v1.4.5 - 2022-04-26(13:33:13 +0000)

### Fixes

- [TR181][Routing]add netmodel query to first check if the ip is set

## Release v1.4.4 - 2022-03-24(10:59:12 +0000)

### Changes

- [GetDebugInformation] Add data model debuginfo in component services

## Release v1.4.3 - 2022-03-17(09:11:26 +0000)

### Changes

- implement iprouter client and iprouter mib

## Release v1.4.2 - 2022-03-15(16:20:37 +0000)

### Other

- [TR181][Routing] add unit tests that override some of the stand alone tests

## Release v1.4.1 - 2022-03-10(09:06:51 +0000)

### Other

- [TR181][Routing] add unit test for the ipvforwarding obj

## Release v1.4.0 - 2022-03-09(08:43:07 +0000)

### New

- [TR-181][Routing] allow router advertisement message

## Release v1.3.0 - 2022-03-04(09:56:02 +0000)

### New

- [TR-181][Routing]handle forwarding routes correctly

## Release v1.2.5 - 2022-03-03(08:14:22 +0000)

### Fixes

- Remove IP flags from netmodel queries to get netdevname

## Release v1.2.4 - 2022-02-25(11:33:09 +0000)

### Other

- Enable core dumps by default

## Release v1.2.3 - 2022-02-18(10:56:22 +0000)

### Fixes

- Fix enable param + other small issues

## Release v1.2.1 - 2022-02-11(15:25:25 +0000)

### Fixes

- [TR181][Routing] parse option 3, decrease index counter

## Release v1.2.0 - 2022-01-28(11:00:02 +0000)

### New

- Add extra router advertisement parameters

## Release v1.1.6 - 2022-01-19(17:11:04 +0000)

### Other

- Add support for RouterInformation

## Release v1.1.5 - 2021-11-23(17:20:31 +0000)

### Other

- [ACL] The Routing manager must have default ACL files configured

## Release v1.1.4 - 2021-11-19(12:05:27 +0000)

### Fixes

- , PCF-405 Disable static routes + enable writing functionality

## Release v1.1.3 - 2021-11-15(09:14:35 +0000)

### Fixes

- Missing mod-sahtrace dependecy in some components

## Release v1.1.2 - 2021-10-20(09:50:34 +0000)

### Fixes

- Fetch only objects, skip templates provided by latest libamxb

## Release v1.1.1 - 2021-10-11(08:56:48 +0000)

### Fixes

- Add missing deps in .gitlab-ci.yml

## Release v1.1.0 - 2021-10-11(08:20:36 +0000)

### New

- Implement `mod-routing-uci` logic
- Create `mod-routing-lin` and define controller interface
- Implement `mod-routing-lin` logic

### Fixes

- Enable parameter not in sync with status
- Remove forwarding instance the netlink says the route is gone
- reset dynamic information when forwarding is set static
- Enable not in the right state for automatic detected routes
- ODL save file issue
- Routing manager does not remove initial IPv\*Forwarding instances

## Release v1.0.6 - 2021-09-27(15:06:28 +0000)

### Fixes

- Fixes missing linker option and missing closing brace

## Release v1.0.5 - 2021-08-26(08:18:34 +0000)

### Changes

- Redefine startup order

## Release v1.0.4 - 2021-08-20(07:27:34 +0000)

### Fixes

- Update ubus when route object is removed from DM

## Release v1.0.3 - 2021-08-17(11:20:44 +0000)

### Changes

- Add child object instances directly too root without use of `libamxb`. Cleanup IPv4 and IPv6 routes handling.

### Other

- Update changelog style
- Create IPv6 Forwarding event based data population

## Release v1.0.2 - 2021-06-23(15:24:57 +0000)

### New

- Add changelog and debian packages folder

## Release v1.0.1 - 2021-06-17(06:34:21 +0000)

### New

- IPv4 Forwarding data model instance creation upon NetDev events

## Release v1.0.0 - 2021-05-27(10:48:53 +0000)

### New

- First delivery of Routing, datamodel and start-up script
